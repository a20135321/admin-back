'use strict'
const express = require('express')
const passport = require("passport");
const helmet = require('helmet')
const cors = require('cors')
const bodyparser = require('body-parser')
const WebSocket = require('ws');
const fs = require('fs')
const https = require('https')
const attachEndpoints = require('../api/backend')
const createWSS = require('../Watson/flow')
const attachJobs = require('../Jobs')
const morgan = require('../Middlewares/morganLogger')

const start = (options) => {
  return new Promise((resolve, reject) => {
    if (!options.repo) {
      reject(new Error('The server must be started with a connected repository'))
    }
    if (!options.port) {
      reject(new Error('The server must be started with an available port'))
    }

    console.log('[INFO] creating app...')
    const app = express();
    app.use(cors());
    app.use(bodyparser.json());
    app.use(bodyparser.urlencoded({extended:true}))
    app.use(morgan)

    app.use(helmet());
    app.use((err, req, res, next) => {
      reject(new Error('Something went wrong!, err:' + err))
      res.status(500).send('Something went wrong!')
    })
    console.log('[INFO] attaching API endpoints...')
    attachEndpoints(app, options)
    console.log('[INFO] API endpoints attached')

    app.use(passport.initialize());
    require("../Middlewares/jwt")(passport);
    //console.log('[INFO] starting cron jobs...')
    //attachJobs()
    //console.log('[INFO] cron jobs started')
    /**/
    const server = https.createServer({
      key: fs.readFileSync('/certs/tls.key', 'utf8'),
      cert: fs.readFileSync('/certs/tls.crt', 'utf8')
    },app).listen(options.port);

    //const server = app.listen(options.port)
    console.log('[INFO] app/server created and listening')
    console.log('[INFO] creating ws server & attaching watson services...')
    const wss = new WebSocket.Server({server});
    createWSS(wss)
    console.log('[INFO] ws server created')
  })
}

module.exports = Object.assign({}, {start})
