const cron = require('node-cron');

module.exports = () => {
  cron.schedule('* * * * *', function() {
    console.log('running a task every minute');
  });
}