const CONFIG = require('./config')
const {google} = require('googleapis');

// Provide the required configuration
const calendarId = CONFIG.calendar_id;

// Google GCalendar API settings
const SCOPES = 'https://www.googleapis.com/auth/calendar';
const calendar = google.calendar({version : "v3"});

const auth = new google.auth.JWT(
  CONFIG.client_email,
  null,
  CONFIG.private_key,
  SCOPES
);

// Your TIMEOFFSET Offset
const TIMEOFFSET = '+05:30';
// Get date-time string for calender
const dateTimeForCalendar = () => {
  let date = new Date();
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  if (month < 10) {
    month = `0${month}`;
  }
  let day = date.getDate();
  if (day < 10) {
    day = `0${day}`;
  }
  let hour = date.getHours();
  if (hour < 10) {
    hour = `0${hour}`;
  }
  let minute = date.getMinutes();
  if (minute < 10) {
    minute = `0${minute}`;
  }
  let newDateTime = `${year}-${month}-${day}T${hour}:${minute}:00.000${TIMEOFFSET}`;
  let event = new Date(Date.parse(newDateTime));
  let startDate = event;
  // Delay in end time is 1
  let endDate = new Date(new Date(startDate).setHours(startDate.getHours()+1));
  return {
    'start': startDate,
    'end': endDate
  }
};

// Insert new event to Google Calendar
const insertEvent = async (event) => {
  try {
    let response = await calendar.events.insert({
      auth: auth,
      calendarId: calendarId,
      resource: event
    });

    if (response['status'] == 200 && response['statusText'] === 'OK') {
      return 1;
    } else {
      return 0;
    }
  } catch (error) {
    console.log(`Error at insertEvent --> ${error}`);
    return 0;
  }
};

let dateTime = dateTimeForCalendar();
// // Event for Google Calendar
let event = {
  'summary': `This is the summary.`,
  'description': `This is the description.`,
   'start': {
       'dateTime': dateTime['start'],
       'timeZone': 'Asia/Kolkata'
   },
   'end': {
       'dateTime': dateTime['end'],
       'timeZone': 'Asia/Kolkata'
   },
  'attendees':[
    {'email':'a20135321@pucp.edu.pe'},
    {'email':'riveratorresrafael@gmail.com'},
  ]
};

insertEvent(event)
  .then((res) => {
         console.log(res);
  }).catch((err) => {
         console.log(err);
  });

/*
// Get all the events between two dates
const getEvents = async (dateTimeStart, dateTimeEnd) => {

  try {
    let response = await GCalendar.events.list({
      auth: auth,
      calendarId: calendarId,
      timeMin: dateTimeStart,
      timeMax: dateTimeEnd,
      timeZone: 'Asia/Kolkata'
    });

    let items = response['data']['items'];
    return items;
  } catch (error) {
    console.log(`Error at getEvents --> ${error}`);
    return 0;
  }
};

// let start = '2020-10-03T00:00:00.000Z';
// let end = '2020-10-04T00:00:00.000Z';

// getEvents(start, end)
//     .then((res) => {
//         console.log(res);
//     })
//     .catch((err) => {
//         console.log(err);
//     });

// Delete an event from eventID
const deleteEvent = async (eventId) => {

  try {
    let response = await GCalendar.events.delete({
      auth: auth,
      calendarId: calendarId,
      eventId: eventId
    });

    if (response.data === '') {
      return 1;
    } else {
      return 0;
    }
  } catch (error) {
    console.log(`Error at deleteEvent --> ${error}`);
    return 0;
  }
};

let eventId = 'hkkdmeseuhhpagc862rfg6nvq4';

deleteEvent(eventId)
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });

 */