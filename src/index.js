'use strict'
require('dotenv').config()
const {EventEmitter} = require('events')
console.log('--- Admin backend Service ---')
const server = require('./server/server')
const repository = require('./repository/repository')
const mediator = new EventEmitter()

process.on('uncaughtException', (err) => {
  console.error('Unhandled Exception', err)
})

process.on('uncaughtRejection', (err, promise) => {
  console.error('Unhandled Rejection', err)
})

console.log('[INFO] starting...')
server.start({
  port: process.env.PORT,
  repo : repository
})
console.log('[INFO] started')

console.log('[INFO] emitting...')
mediator.emit('boot.ready')
console.log('[INFO] emitted')
