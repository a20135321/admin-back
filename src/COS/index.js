const COS = require('ibm-cos-sdk');
const listAllObjects = require('../Helpers/listFilesCOS');
const multer = require('multer');
const multerS3 = require('multer-s3-transform')
const fetch = require('node-fetch');
const sharp = require('sharp');
const path = require('path');

const defaultEndpoint = 's3.us-south.cloud-object-storage.appdomain.cloud';
const s3Options = {
  apiKeyId: process.env.COS_API_KEY,
  serviceInstanceId: process.env.COS_INSTANCE_ID,
  region: 'us-south',
  endpoint: new COS.Endpoint(defaultEndpoint),
};
const buckets = {
  dev:"rebeca-images",
  test:"rebeca-test"
}
const bucket = buckets[process.env.PG_SCHEMA]

console.log('[INFO] connecting COS...')
const s3 = new COS.S3(s3Options);
console.log('[INFO] COS connected...')

const uploadLog = (jsonObj) =>{
  const params = {
    Bucket: bucket,
    Key: 'logs/'+Date.now().toString()+'.json',
    Body: Buffer.from(JSON.stringify(jsonObj), 'utf8'),
  };
  s3.putObject(params, function(err, data) {
    if (err) console.log(err, err.stack);
  });
}

const uploadImage = (stream) =>{
  const params = {
    Bucket: bucket,
    ACL:'public-read',
    Key: 'images/thumb_'+Date.now().toString()+'.jpg',
    Body: stream,
  };
  s3.putObject(params, function(err, data) {
    if (err) console.log(err, err.stack);
  });
}

const uploadAppLabels = (content,name) =>{
  const key = 'labels/'+name+'.csv'
  const params = {
    Bucket: bucket,
    ACL:'public-read',
    Key: key,
    Body: Buffer.from(content, 'utf8'),
  };
  s3.putObject(params, function(err, data) {
    if (err) console.log(err, err.stack);
  });
  return "https://"+bucket+".s3.us-south.cloud-object-storage.appdomain.cloud/"+key;
}

const deleteImages = (UriList) =>{
  const obj = []
  UriList.forEach(elm =>{
    obj.push({ Key: elm })
  })
  const params = {
    Bucket: bucket,
    Delete: {
      Objects: obj,
      Quiet: false
    }
  };
  s3.deleteObjects(params, function(err, data) {
    if (err) console.log(err, err.stack);
  });
}

const listImages = async () =>{
  const params = {
    bucket: bucket,
    prefix: 'images/',
  };
  return await new Promise(resolve => {
      listAllObjects(s3,params, function(err, data) {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Success", data.length);
          console.log(data.map(elm=>elm.Key))
          resolve(data)
        }
      });
  });
}

const fileFilter = (req, file, cb) => {
  if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
}
const upload = multer({
  fileFilter,
  storage: multerS3({
    s3: s3,
    acl: 'public-read',
    bucket: bucket,
    shouldTransform: function (req, file, cb) {
      cb(null, /^image/i.test(file.mimetype))
    },
    transforms: [{
      id: 'original',
      key: function (req, file, cb) {
        cb(null, 'images/'+Date.now().toString()+path.parse(file.originalname).name+'.jpeg')
      },
      transform: function (req, file, cb) {
        cb(null, sharp({ failOnError: false }).toFormat('jpeg'))
      }
    }, {
      id: 'thumbnail',
      key: function (req, file, cb) {
        cb(null, 'images/thumb_'+Date.now().toString()+path.parse(file.originalname).name+'.jpeg')
      },
      transform: function (req, file, cb) {
        cb(null, sharp({ failOnError: false }).resize({width:512,fit:sharp.fit.contain}).toFormat('jpeg'))
      }
    }],
    //key: function (req, file, cb) {
    //  cb(null, 'images/'+Date.now().toString()+file.originalname);
    //}
  })
});

module.exports = {
  upload,
  uploadLog,
  uploadImage,
  uploadAppLabels,
  deleteImages,
  listImages
}