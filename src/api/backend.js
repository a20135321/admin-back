'use strict'
const api_header = process.env.API_ROUTE;
const {upload} = require('../COS')
const {check} = require('express-validator');
const validate = require('../Middlewares/validate')
const authenticate = require('../Middlewares/authenticate')
const {refreshJWT} = require('../Helpers/encrypt')
const {synthesize} = require('../Watson/TTS')
const PDGGen = require('../PDFGen/generator')
/*
const verifyToken = async (req, res, next) => {
  const { token } = req.headers;
  if (!token) {
    errorMessage.error = 'Token not provided';
    return res.status(status.bad).send(errorMessage);
  }
  try {
    const decoded =  jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = {
      user_id: decoded.user_id,
      email: decoded.email,
      username: decoded.username
    };
    next();
  } catch (error) {
    errorMessage.error = 'Authentication Failed';
    return res.status(status.unauthorized).send(errorMessage);
  }
};
 */

module.exports = (app, options) => {
  let repo = options.repo;
  //-------LIVENESS PROBE-------
  app.get(api_header+'ready_live',repo.ready_live);
  //-------CLIENTS-------
  app.get(api_header+'clients/list',repo.client.getAllClients);
  app.post(api_header+'clients/add', repo.client.register);
  app.post(api_header+'clients/login', repo.client.login);
  app.post(api_header+'clients/preferences/add', repo.client.addClientPreferences);
  app.post(api_header+'clients/get/filter',repo.client.getClientsByFilter);
  app.post(api_header+'clients/get',repo.client.getClientByID);
  app.post(api_header+'clients/delete/email',repo.client.deleteUsersByEmail);
  app.post(api_header+'clients/check', repo.client.checkEmailExists);
  app.post(api_header+'clients/update', repo.client.updateClient);
  //-------ADMINS-------
  app.post(api_header+'admins/add',upload.any(),repo.admin.register);
  app.post(api_header+'user/login',repo.admin.login);
  app.post(api_header+'user/recover',repo.admin.sendRecoverEmail);
  app.post(api_header+'user/reset',repo.admin.resetPassword);
  app.post(api_header+'admins/edit',upload.any(),repo.admin.editAdmin);
  app.post(api_header+'admins/delete',repo.admin.deleteAdminsByID);
  app.post(api_header+'admins/get/filter', repo.admin.getMyAdmins);
  app.post(api_header+'admins/get/editable',repo.admin.getAdminEditableDetails);
  app.post(api_header+'admins/update/status', repo.admin.updateAdminStatus);
  //-------PROVIDERS-------
  app.post(api_header+'operator/add',upload.any(),repo.provider.addProviderOrOperator);
  app.post(api_header+'provider/add',upload.any(),repo.provider.addProviderOrOperator);
  app.post(api_header+'provider/edit',repo.provider.editProviderOrOperator);
  app.post(api_header+'provider/delete',repo.provider.deleteProvidersByID);
  app.post(api_header+'provider/selector', repo.provider.getSelectorProviders);
  app.post(api_header+'provider/get/filter', repo.provider.getMyProviders);
  app.post(api_header+'provider/get/editable',repo.provider.getProviderEditableDetails);
  app.post(api_header+'provider/update/status', repo.provider.updateProviderStatus);
  app.post(api_header+'provider/update/languages', repo.provider.updateProviderLanguages);
  //-------POIs-------
  app.post(api_header+'poi/add',repo.poi.addPOI);
  app.post(api_header+'activity/poi/delete',repo.poi.deletePOIsByID);
  app.post(api_header+'activity/poi/get/filter',repo.poi.getPOIsByFilter);
  app.post(api_header+'activity/poi/get/editable',repo.poi.getPOIEditableDetails);
  app.post(api_header+'activity/poi/edit',repo.poi.editPOI);
  app.post(api_header+'poi/selector', repo.poi.getSelectorPOI);
  //-------SERVICES-------
  app.post(api_header+'service/accommodation/add',upload.any(),repo.service.addAccommodationService);
  app.post(api_header+'service/accommodation/questions/get',repo.service.getQuestionsFromAccommodation);
  app.post(api_header+'service/accommodation/distribution/get',repo.service.getDistributionFromAccommodation);
  app.post(api_header+'service/restaurant/add',upload.any(),repo.service.addRestaurantService);
  app.post(api_header+'service/other/add',upload.any(),repo.service.addOtherService);
  app.post(api_header+'service/accommodation/get/range',repo.service.getAccommodationsInDateRange);
  app.post(api_header+'service/restaurants/get',repo.service.getRestaurantsInDate);
  app.post(api_header+'activity/get/filter',repo.service.getServicesByFilter);
  app.post(api_header+'service/get/editable',repo.service.getServiceEditableDetails);
  app.post(api_header+'service/edit',upload.any(),repo.service.editService);
  app.post(api_header+'activity/delete',repo.service.deleteServicesByID);
  app.post(api_header+'activity/status/update',repo.service.updateServicesStatus);
  app.post(api_header+'service/get/info',repo.service.getServiceInfo);
  //-------TOURS-------
  app.post(api_header+'activity/tour/get/filter',repo.tour.getToursByFilter);
  app.post(api_header+'activity/tour/get/range',repo.tour.getToursInDateRange);
  app.post(api_header+'activity/tour/get/info', repo.tour.getTourInfoByID);
  app.post(api_header+'tour/add',upload.any(),repo.tour.addTour);
  app.post(api_header+'tour/get/editable', repo.tour.getTourEditableDetails);
  //-------USER TRIPS-------
  app.post(api_header+'clients/trip/get', repo.clientTrip.getMyUserTrips);
  app.post(api_header+'clients/trip/appoint', repo.clientTrip.newClientTrip);
  app.post(api_header+'clients/trip/update', repo.clientTrip.updateClientTrip);
  app.post(api_header+'clients/trip/schedule/set', repo.clientTrip.setClientTripSchedule);
  app.post(api_header+'clients/trip/schedule/get', repo.clientTrip.getClientTripSchedule);
  app.post(api_header+'clients/trip/get/filter', repo.clientTrip.getClientsTripsByFilter);
  app.post(api_header+'clients/trip/delete', repo.clientTrip.deleteClientTripsByID);
  //-------LABELS-------
  app.get(api_header+'resources/labels/update', repo.label.updateLabelsVersion);
  app.get(api_header+'resources/labels/consult', repo.label.checkForNewVersions);
  app.post(api_header+'labels/app/localize', repo.label.getAppLocalizedLabels);
  app.post(api_header+'labels/web/localize', repo.label.getWebLocalizedLabels);
  app.post(api_header+'labels/items/get/selector',repo.label.getLabelItemsSelector);
  app.post(api_header+'labels/spaces/get/selector',repo.label.getLabelSpacesSelector);
  //-------GROUP PERMISSIONS-------
  app.post(api_header+'permission/group/get', repo.permission.getGroups);
  app.post(api_header+'permission/group/user/get', repo.permission.getGroupsOfUser);
  app.post(api_header+'permission/group/user/update', repo.permission.updateGroupsOfUser);
  app.post(api_header+'permission/group/get/single', repo.permission.getPermissionsInGroup);
  app.post(api_header+'permission/group/update', repo.permission.updatePermissionInGroup);
  app.post(api_header+'permission/group/status', repo.permission.updateAccessGroupStatus);
  app.post(api_header+'permissions/group/delete', repo.permission.deleteAccessGroup);
  app.post(api_header+'permissions/group/add', repo.permission.addAccessGroup);
  app.post(api_header+'permissions/add', repo.permission.addPermission);
  app.post(api_header+'permissions/delete', repo.permission.deletePermissions);
  //-------WATSON-------
  app.post(api_header+'watson/questions/get/filter',repo.watson.getWatsonQuestions);
  app.post(api_header+'watson/questions/get/selector',repo.watson.getWatsonQuestionsSelector);
  app.post(api_header+'watson/questions/delete',repo.watson.deleteQuestions);
  app.post(api_header+'watson/questions/upsert',repo.watson.upsertQuestion);
  app.post(api_header+'watson/assistant/ask',repo.client.askWatsonAssistant);
  app.post(api_header+'watson/assistant/askV2',repo.client.askWatsonAssistantV2);
  app.get(api_header+'watson/TTS',synthesize);
  //-------AUTH-------
  app.post(api_header+'token/refresh',authenticate,refreshJWT);
  //-------HELPERS-------
  app.get(api_header+'image/resize',repo.resizeImage);
  app.post(api_header+'multilanguage/translate',repo.translateMultiLanguageTab);
  //-------TESTING-------
  app.post(api_header+'watson/test',
    check('data.question').not().isEmpty().withMessage("holi1"),
    check('data.language_code').not().isEmpty().withMessage("holi2"),
    validate,authenticate,repo.test);
  app.get(api_header+'generate/pdf/:user_trip_id',PDGGen.generatePDF);
  app.get(api_header+'test/nearest',repo.getNearestPlaces);
}
