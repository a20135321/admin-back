const moment  = require('moment')
const dbQuery  = require('../config/datos-relacionales')
const SG = require('../Sendgrid/mailer')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const {consultWatson,watsonTest,consultWatsonV2} = require('../Watson/Assistant/index')
const {generateJWT,comparePassword} = require('../Helpers/encrypt')
const cloneDeep = require('clone-deep');
const axios = require('axios')
const {getDistance} = require('geolib');

const deleteMultipleByKey = async (req, res, table, key_name,undeletable=[],affected_table = undefined,origin_key=undefined,target_key = undefined) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  try {
    await deleteMultipleByKeyUnwrapped(body,table,key_name,undeletable,affected_table,origin_key,target_key)
    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const deleteMultipleByKeyUnwrapped = async (key_list, table, key_name, undeletable=[],affected_table = undefined,origin_key=undefined,target_key = undefined, conditions=undefined) =>{
  let origin_values;
  for(let i=0;i<key_list.length;i++){
    if (undeletable.includes(key_list[i])){
      continue
    }
    origin_values = await deleteAllBySingleKey(table,key_name,key_list[i],origin_key)
    for(let j=0;affected_table && j<origin_values.length;j++){
      await deleteAllBySingleKey(affected_table,target_key,origin_values[j][origin_key],conditions)
    }
  }
}

const deleteAllBySingleKey = async (table, key_name, key_value, returned_key=undefined,conditions=undefined) =>{
  let queryText = "delete from "+table+" where "+key_name+" = $1";
  queryText += conditions?' where '+conditions:"";
  queryText += returned_key?' returning '+returned_key:"";
  const {rows} = await dbQuery.query(queryText,[key_value]);
  if (returned_key) return rows
}

//true if available
const checkFieldExists = async (field,table,table_id,value) =>{
  const queryText = "select count("+table_id+") from "+table+" where "+field+" = $1";
  try {
    const dbResponse = await dbQuery.query(queryText,[value]);
    return dbResponse.rows[0].count === '0';
  } catch (error) {
    return false;
  }
}
/*
const saveLocalizedData = async (languages,object,table,text_key,entity_id) =>{
  const queryText = "insert into "+table+" (entity_id,language_code,text_key,created_date,text_value) values ($1,$2,$3,current_date,$4)";
  let language_code,queryValues;
  for(let i=0;i<languages.length;i++){
    language_code = languages[i].key;
    queryValues = [entity_id,language_code,text_key,object[text_key+'-'+language_code]]
    await dbQuery.query(queryText,queryValues);
  }
}
 */

const getLocalizedData = async (table,text_key,entity_id) =>{
  const queryText = "select * from "+table+" where text_key=$1 and entity_id=$2";
  return await dbQuery.query(queryText,[text_key,entity_id]);
}

const saveLocalizedData = async (languages,object,table,text_key,entity_id) =>{
  //const queryText1 = "delete from "+table+" where entity_id=$1 and language_code=$2 and text_key=$3";
  const queryText1 = "delete from "+table+" where entity_id=$1 and text_key=$2";
  const queryText2 = "insert into "+table+" (entity_id,language_code,text_key,created_date,text_value) values ($1,$2,$3,current_date,$4)";
  //const queryText3 = "update "+table+" set text_value = $1, created_date = CURRENT_DATE where entity_id=$2 and language_code=$3 and text_key=$4";
  let language_code,queryValues,localized_text_key;

  await dbQuery.query(queryText1,[entity_id,text_key]);
  for(let i=0;i<languages.length;i++){
    language_code = languages[i].key;
    localized_text_key = text_key+'-'+language_code
    if (object.hasOwnProperty(localized_text_key)){
      //await dbQuery.query(queryText1,[entity_id,language_code,text_key]);
      queryValues = [entity_id,language_code,text_key,object[localized_text_key]]
      await dbQuery.query(queryText2,queryValues);
    }
  }
}

const addLocalizedQuery = (queryText,queryValue_number,parent_id,localized_table,localized_abbrev,text_key) =>{
  queryText += " left join (select distinct on (text_key,entity_id) text_value,language_code,entity_id from "+localized_table+" where language_code in ('en',$"+queryValue_number+") and text_key='"+text_key+"' order by text_key,entity_id,language_code desc) as "+localized_abbrev+" on "+localized_abbrev+".entity_id="+parent_id;
  return queryText
}

const updateEntityStatus = async (req, res, table, id_field) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "update "+table+" set "+table+"_status = $1 where "+id_field+" = $2";
  try {
    await dbQuery.query(queryText,[body.new_status,body.id]);
    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const updateTableSingleField = async (req, res, table, field, id_field) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "update "+table+" set "+field+" = $1 where "+id_field+" = $2";
  try {
    await dbQuery.query(queryText,[body.value,body.id]);
    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const simpleTableSelect = async (req, res, table, filter, value) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const queryText = "select * from "+table+" where "+filter+"='"+value+"'";
  try {
    const dbResponse = await dbQuery.query(queryText,[]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const defaultEmptyValidation = async () =>{

}

const getPermissionsOfUser = async (user_id) =>{
  const permitsAndGroups = {};
  let dbResponse;
  const queryText1 = "select distinct ag.* from user_access u inner join access_groups ag on u.group_id=ag.group_id" +
    " where ag.enabled and u.user_id=$1 order by ag.group_name asc"
  const queryText2 = "select distinct p.* from user_access u inner join access_groups ag on u.group_id=ag.group_id" +
    " inner join group_permissions gp on ag.group_id=gp.group_id inner join permissions p" +
    " on gp.permission_id=p.permission_id where ag.enabled and u.user_id=$1 order by p.permission_name asc"
  dbResponse = await dbQuery.query(queryText1,[user_id]);
  permitsAndGroups.groups = (dbResponse.rows[0] === undefined)?[]:dbResponse.rows;

  dbResponse = await dbQuery.query(queryText2,[user_id]);
  permitsAndGroups.permissions = (dbResponse.rows[0] === undefined)?[]:dbResponse.rows.map(elm => elm.permission_name);
  return permitsAndGroups;
}

const assignAccessGroupsToUser = async (user_id,group_ids) =>{
  const queryText = "insert into user_access (user_id,group_id) values ($1,$2)";
  for(let i=0;i<group_ids.length;i++){
    await dbQuery.query(queryText,[user_id,group_ids[i]]);
  }
}

const removeAccessGroupsFromUser = async (user_id,group_ids) =>{
  const queryText = "delete from user_access where user_id=$1 and group_id=$2";
  for(let i=0;i<group_ids.length;i++){
    await dbQuery.query(queryText,[user_id,group_ids[i]]);
  }
}

const getSingleImageRef = (type,files,field_name) =>{
  let image_ref,thumb_ref;
  if (type==='user'){
    image_ref = "https://rebeca-images.s3.us-south.cloud-object-storage.appdomain.cloud/1610770369732defaultUser.png"
  }else if(type==='question' || type==='distribution'){
    image_ref = null
  }else{
    image_ref = "https://rebeca-images.s3.us-south.cloud-object-storage.appdomain.cloud/1610833705817defaultImage.png";
  }
  thumb_ref = image_ref
  const file = files.find(elm => elm.fieldname===field_name)
  if (file!==undefined){
    image_ref = file.transforms.find(elm => elm.id==='original').location
    thumb_ref = file.transforms.find(elm => elm.id==='thumbnail').location
  }
  return [image_ref,thumb_ref]
}

const checkImageRef = (files,field_name) =>{
  const file = files.find(elm => elm.fieldname===field_name)
  return file!==undefined
}

const saveActivitySchedule = async (body,activity_id) =>{
  const queryText = "insert into activity_schedule (start_time,end_time,date,activity_id) values ($1,$2,$3,$4);";
  const queryText2 = "insert into schedule_brief (activity_id,calendar) values ($1,$2)"
  let initial_date,final_date,next_date,hour_list,start_time,end_time,available_days,queryValues;
  let schedule_brief_instance, hour_ranges;
  for(let i=0;i<body.dates.length;i++){
    initial_date = moment(body.dates[i].datePick[0]);
    final_date = moment(body.dates[i].datePick[1]).add(1, 'days');
    available_days = body.dates[i].weeke;
    hour_list = [];
    if (body.dates[i].hour_list != undefined){
      body.dates[i].hour_list.forEach(elm => {
        hour_list.push({timePick:[moment(elm.timePick[0]).format('HH:mm:ss'),moment(elm.timePick[1]).format('HH:mm:ss')]})
      })
    }
    if (hour_list.length===0) hour_list.push({timePick:['00:00:00','23:59:59']})

    schedule_brief_instance = initial_date.format('YYYY-MM-DD')+"/"+moment(body.dates[i].datePick[1]).format('YYYY-MM-DD')+
      "%"+available_days.join()+"%"

    hour_ranges = []
    hour_list.forEach(elm => {
      hour_ranges.push(elm.timePick[0]+"/"+elm.timePick[1])
    })
    schedule_brief_instance += hour_ranges.join()
    await dbQuery.query(queryText2,[activity_id,schedule_brief_instance]);

    next_date = initial_date;
    while (next_date.format('YYYY-MM-DD')!==final_date.format('YYYY-MM-DD')){
      if (available_days.includes(next_date.format('d'))){
        for(let j=0;j<hour_list.length;j++){
          start_time = hour_list[j].timePick[0];
          end_time = hour_list[j].timePick[1];
          queryValues = [start_time,end_time,next_date.format('YYYY-MM-DD'),activity_id];
          await dbQuery.query(queryText,queryValues);
        }
      }
      next_date = next_date.add(1, 'days');
    }
  }
}

const saveTipsAndRecommendations = async (body,associated_entity,associated_id) =>{
  const queryText = "insert into tip (associated_entity,associated_id,type,is_push,frequency,url,is_tip)" +
    " values ($1,$2,$3,$4,$5,$6,$7) returning tip_id";
  let queryValues,dbResponse,tip_id,recom_id,tip,recom;

  if (body.tips!==undefined){
    for(let i=0;i<body.tips.length;i++){
      tip = body.tips[i];
      queryValues = [associated_entity,associated_id,tip.type,tip.is_push,tip.frequency,tip.url,true]
      dbResponse = await dbQuery.query(queryText,queryValues);
      tip_id = dbResponse.rows[0].tip_id;
      await saveLocalizedData(body.languages,tip,'tip_localized','message',tip_id);
      await saveLocalizedData(body.languages,tip,'tip_localized','title',tip_id);
    }
  }

  if (body.recoms!==undefined){
    for(let i=0;i<body.recoms.length;i++){
      recom = body.recoms[i];
      queryValues = [associated_entity,associated_id,recom.type,recom.is_push,recom.frequency,recom.url,false]
      dbResponse = await dbQuery.query(queryText,queryValues);
      recom_id = dbResponse.rows[0].tip_id;
      await saveLocalizedData(body.languages,recom,'tip_localized','message',recom_id);
      await saveLocalizedData(body.languages,recom,'tip_localized','title',recom_id);
    }
  }
}

const addGenericService = async (req, res, category) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = JSON.parse(req.body.data);
  const queryText1 = "insert into activity (prov_id,title,latitude,longitude,website,cost,activity_status,category,country,city,address,max_persons,visit_start_time,visit_end_time,currency) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15) returning activity_id";
  const queryText2 = "insert into activity_images (activity_id,image_ref,thumb_ref) values ($1,$2,$3)"
  const queryText3 = "insert into questions_answers (activity_id,intent,image_ref,thumb_ref) values ($1,$2,$3,$4) returning qa_id"

  const queryText4 = "insert into accommodation_distribution (activity_id,floor,space,image_ref,thumb_ref) values ($1,$2,$3,$4,$5) returning distribution_id"
  const queryText5 = "insert into items (distribution_id,image_ref,thumb_ref,quantity,value,entity) values ($1,$2,$3,$4,$5,$6) returning item_id"

  let dbResponse, queryValues, image_ref,thumb_ref,QA,spaces,items,space,item,distribution_id;

  try {
    await dbQuery.query("BEGIN");

    const country = (body.hasOwnProperty('country'))?body.country.label:"Undef";
    const city = (body.hasOwnProperty('city'))?body.city.label:"Undef";
    const visit_start_time = (body.hasOwnProperty('visiting_hours'))?moment(body.visiting_hours[0]).format('HH:mm:ss'):undefined;
    const visit_end_time = (body.hasOwnProperty('visiting_hours'))?moment(body.visiting_hours[1]).format('HH:mm:ss'):undefined;
    const max_persons = (body.hasOwnProperty('max_persons'))?body.max_persons:undefined;
    const service_category = (category===undefined)?body.category:category;
    const {latitude,longitude} = body
    queryValues = [body.provider.value,body.title,latitude,longitude,body.website,body.cost,'pending',service_category,country,city,body.address,max_persons,visit_start_time,visit_end_time,body.currency];

    dbResponse = await dbQuery.query(queryText1,queryValues);
    const activity_id = dbResponse.rows[0].activity_id;

    for(let i=0;i<body.total_images;i++){
      [image_ref,thumb_ref] = getSingleImageRef('service',req.files,'activity'+i)
      queryValues = [activity_id,image_ref,thumb_ref]
      await dbQuery.query(queryText2,queryValues);
    }

    if (latitude && longitude) await saveNearestPlaces(latitude,longitude);
    await saveLocalizedData(body.languages,body,'activity_localized','activity_description',activity_id);
    await saveLocalizedData(body.languages,body,'activity_localized','activity_short_description',activity_id);
    await saveActivitySchedule(body,activity_id)

    await saveTipsAndRecommendations(body,"activity",activity_id);

    if (body.questions!==undefined){
      for(let i=0;i<body.questions.length;i++){
        QA = body.questions[i];
        [image_ref,thumb_ref] = getSingleImageRef('question',req.files,'question'+i);
        queryValues = [activity_id,QA.quest.key,image_ref,thumb_ref]
        dbResponse = await dbQuery.query(queryText3,queryValues);
        await saveLocalizedData(body.languages,QA,'answer_localized','answer',dbResponse.rows[0].qa_id);
      }
    }

    if (body.floors!==undefined){
      for(let i=0;i<body.floors.length;i++){
        spaces = body.floors[i].spaces;
        distribution_id = undefined
        for(let j=0;j<spaces.length;j++){
          space = spaces[j];
          [image_ref,thumb_ref] = getSingleImageRef('distribution',req.files,'space_'+i+'_'+j);
          queryValues = [activity_id,i,space.space,image_ref,thumb_ref];
          dbResponse = await dbQuery.query(queryText4,queryValues);
          distribution_id = dbResponse.rows[0].distribution_id;
          await saveLocalizedData(body.languages,space,'activity_localized','space_description',distribution_id);

          items = space.items?space.items:[]
          for(let k=0;k<items.length;k++){
            item = items[k];
            [image_ref,thumb_ref] = getSingleImageRef('distribution',req.files,'item_'+i+'_'+j+'_'+k);
            queryValues = [distribution_id,image_ref,thumb_ref,item.quantity,item.value,item.entity];
            dbResponse = await dbQuery.query(queryText5,queryValues);
            await saveLocalizedData(body.languages,item,'item_localized','description',dbResponse.rows[0].item_id);
          }
        }
        if (distribution_id!==undefined) await saveLocalizedData(body.languages,body.floors[i],'activity_localized','floor_description',distribution_id);
      }
    }

    await dbQuery.query("COMMIT");

    return res.status(status.created).send(sMessage);
  } catch (error) {
    console.log(error)
    await dbQuery.query("ROLLBACK");
    return res.status(status.error).send(eMessage);
  }
}

const notifyAdminStatusChanged = async (admin_id,new_status) =>{
  const queryText = "select a.admin_status,u.email from admin a inner join users u on a.admin_id=u.user_id where a.admin_id=$1";
  const dbResponse = await dbQuery.query(queryText,[admin_id]);
  const prev_status = dbResponse.rows[0].admin_status;
  const email = dbResponse.rows[0].email;
  if (new_status!==prev_status){
    SG.sendEmailNotification("Your admin status changed to:"+new_status.toUpperCase(),email)
  }
}

const notifyProviderStatusChanged = async (provider_id,new_status) =>{
  const queryText1 = "select provider_status from provider where prov_id=$1";
  const queryText2 = "select u.email from provider p inner join admin a on a.provider_id=p.prov_id inner join users u " +
    "on a.admin_id=u.user_id where p.prov_id=$1";
  let dbResponse = await dbQuery.query(queryText1,[provider_id]);
  const prev_status = dbResponse.rows[0].provider_status;
  if (new_status!==prev_status){
    dbResponse = await dbQuery.query(queryText2,[provider_id]);
    dbResponse.rows.forEach(elm => {
      SG.sendEmailNotification("Your provider status changed to: "+new_status.toUpperCase(),elm.email)
    })
  }
}

const sendInquiryToProvider = async (activity_id, intent, client_id) => {
  const queryText1 = "select u.email,u.name from activity ac inner join admin ad on ac.prov_id=ad.provider_id inner join users u on ad.admin_id=u.user_id" +
    " where ac.activity_id=$1 and ad.is_company";
  const queryText2 = "select en from watson_questions where intent=$1";
  const queryText3 = "select email,name,last_name from users where user_id=$1";
  const {rows:admins} = await dbQuery.query(queryText1,[activity_id])
  if (admins[0]!==undefined){
    const {rows:questions} = await dbQuery.query(queryText2,[intent])
    if (questions[0]!==undefined){
      const {rows:clients} = await dbQuery.query(queryText3,[client_id])
      const message = `Hi ${admins[0].name}, \nYour client, ${clients[0].name}, made a consult about '${questions[0].en}' and we could not find an answer in your provided data.\nPlease keep in contact with ${clients[0].email} to solve his/her doubts!`
      SG.sendEmailNotification(message,admins[0].email)
    }
  }
}

const askAssistant = async (question,client_id,activity_id,language_code) =>{
  console.log("QUESTION:",question);
  const queryText1 = "select conversation_context from client where client_id=$1"
  let queryText2 = "select al.text_value as answer,al.language_code,qa.image_ref,qa.thumb_ref from questions_answers qa "
  queryText2 = addLocalizedQuery(queryText2,1,'qa.qa_id','answer_localized','al','answer')
  queryText2 += " where qa.activity_id=$2 and qa.intent=$3"
  const queryText3 = "update client set conversation_context=$1 where client_id=$2"
  const queryText4 = "update watson_questions set calls=calls+1 where intent = $1"
  const missing={
    en:"I couldn't find an answer to your question. *sad",
    es:"No pude encontrar una respuesta a tu pregunta. *sad",
    it:"Non sono riuscito a trovare una risposta alla tua domanda. *sad"
  }
  let dbResponse, watsonResponse;

  dbResponse = await dbQuery.query(queryText1,[client_id])
  const conversation_context = dbResponse.rows[0].conversation_context

  watsonResponse = await consultWatson(language_code,JSON.parse(conversation_context),question)
  const intent = watsonResponse.intent
  if (intent!==undefined){
    if(activity_id!==undefined){
      dbResponse = await dbQuery.query(queryText2,[language_code,activity_id,intent])
      await dbQuery.query(queryText3,[JSON.stringify(watsonResponse.context),client_id])
      await dbQuery.query(queryText4,[intent])
      if (dbResponse.rows[0]!==undefined){
        return dbResponse.rows[0]
      }else{
        await sendInquiryToProvider(activity_id,intent,client_id)
        return {answer:missing[language_code]}
      }
      //return (dbResponse.rows[0]!==undefined)?dbResponse.rows[0]:{answer:missing[language_code]};
    }
    return {answer:missing[language_code]};
  }else{
    return {answer:watsonResponse.text}
  }
}

const getDistributionFromAccommodation = async (language_code,activity_id) =>{
  let queryText1 = "select ad.*,ll.*,sl.text_value as space_description from accommodation_distribution ad inner join labels_localized ll on ad.space=ll.text_key "
  queryText1 = addLocalizedQuery(queryText1,1,'ad.distribution_id','activity_localized','sl','space_description')
  queryText1 += " where ad.activity_id=$2 and ll.platform='web'"
  let queryText2 = "select coalesce(il.text_value,ll."+language_code+") as description, i.thumb_ref, i.quantity, i.value, a.currency from items i inner join labels_localized ll on i.entity=ll.text_key"
  queryText2 = addLocalizedQuery(queryText2,1,'i.item_id','item_localized','il','description')
  queryText2 += " cross join activity a where ll.platform='web' and i.distribution_id=$2 and a.activity_id=$3"
  let queryText3 = "select fl.text_value as floor_description from accommodation_distribution ad "
  queryText3 = addLocalizedQuery(queryText3,1,'ad.distribution_id','activity_localized','fl','floor_description')
  queryText3 += " where ad.distribution_id=ANY($2) and fl.text_value is not null"
  const distribution = {floors:[]}
  const {rows} = await dbQuery.query(queryText1,[language_code,activity_id])
  if (rows[0] === undefined) return undefined //
  let floor,spaces,space,dbResponse;
  for(let i=0;i<=rows.reduce((a,b)=>a.floor>b.floor?a:b).floor;i++){
    floor = {floor:i+1, spaces:[]}
    spaces = rows.filter(elm => elm.floor===i)
    dbResponse = await dbQuery.query(queryText3,[language_code,[spaces.map(elm=>elm.distribution_id)]])
    floor.description = dbResponse.rows[0]?dbResponse.rows[0].floor_description:null
    for(let j=0;j<spaces.length;j++){
      space = {space:spaces[j][language_code],space_key:spaces[j].space,thumb_ref:spaces[j].thumb_ref,description:spaces[j].space_description}
      dbResponse = await dbQuery.query(queryText2,[language_code,spaces[j].distribution_id,activity_id])
      space.items = (dbResponse.rows[0]!==undefined)?dbResponse.rows:[]
      floor.spaces.push(space)
    }
    distribution.floors.push(floor)
  }
  return distribution;
}
/*
const askAssistantV3 = async (question,client_id,language_code,parameters) =>{
  const queryText1 = "select conversation_context from client where client_id=$1"
  const queryText2 = "update client set conversation_context=$1 where client_id=$2"

  const missing={
    en:"I couldn't find an answer to your question",
    es:"No pude encontrar una respuesta a tu pregunta",
    it:"Non sono riuscito a trovare una risposta alla tua domanda"
  }
  const confirmation={
    en:{yes:'Yes, there is',no:'No, there is not'},
    es:{yes:'Sí, se cuenta con ello',no:'No se cuenta con ello'},
    it:{yes:'Sì, si conta',no:'Non si conta'}
  }
  const action_speak = {
    "name": "speak",
    "parameters": {
      "text": missing[language_code]
    }
  }
  const action_raw_data = {
    "name": "raw_data",
    "parameters": {
      "data":{}
    }
  }
  const action_gesture = {
    "name": "gesture",
    "parameters": {
      "gestures": ["sad"]
    }
  }

  let action_parameters,queryText3,dbResponse,queryValues=[];
  let intent,floor,space,item;

  dbResponse = await dbQuery.query(queryText1,[client_id])
  const conversation_context = dbResponse.rows[0]?dbResponse.rows[0].conversation_context:null
  const {app_actions, back_actions, context} = await consultWatsonV2(language_code,JSON.parse(conversation_context),question)
  await dbQuery.query(queryText2,[JSON.stringify(context),client_id])
  console.log(back_actions)
  for(let i=0;i<back_actions.length;i++){
    action_parameters = back_actions[i].parameters;
    switch (back_actions[i].name){
      case 'query':
        switch (action_parameters.type){
          case 'qa':
            queryText3 = "select al.text_value as text,al.language_code,qa.image_ref,qa.thumb_ref from questions_answers qa "
            queryText3 = addLocalizedQuery(queryText3,1,'qa.qa_id','answer_localized','al','answer')
            queryText3 += " where qa.activity_id=$2 and qa.intent=$3"
            intent = action_parameters.intent + ((action_parameters.entity!==undefined)?"_"+action_parameters.entity:"");
            dbResponse = await dbQuery.query(queryText3,[language_code,parameters.activity_id,intent])
            if (dbResponse.rows[0]!==undefined){
              action_speak.parameters.text = dbResponse.rows[0].text
              action_raw_data.parameters.data = dbResponse.rows[0]
            }else{
              app_actions.splice(app_actions.findIndex((elm => elm.name==='redirect')),1)
              if (app_actions.findIndex((elm => elm.name==='gesture'))!==-1) app_actions.splice(app_actions.findIndex((elm => elm.name==='gesture')),1)
              app_actions.push(action_gesture)
              //await sendInquiryToProvider(parameters.activity_id,intent,client_id)
            }
            app_actions.push(action_speak)
            app_actions.push(action_raw_data)
            break;
          case 'inventory':
            intent = action_parameters.intent
            item = action_parameters.item
            floor = action_parameters.floor
            space = action_parameters.space
            if (item===undefined){
              action_raw_data.parameters.data = await getDistributionFromAccommodation(language_code,parameters.activity_id)
              app_actions.push(action_raw_data)
              continue
            }
            app_actions.splice(app_actions.findIndex((elm => elm.name==='redirect')),1)
            app_actions.splice(app_actions.findIndex((elm => elm.name==='speak')),1)
            queryText3 = "select sum(i.quantity) from items i inner join accommodation_distribution ad on i.distribution_id=ad.distribution_id where ad.activity_id=$1 and i.entity=$2"
            queryValues = [parameters.activity_id,item]
            if (space!==undefined) queryValues.push(space)
            queryText3 += (space!==undefined)?" and ad.space=$"+queryValues.length:""
            if (floor!==undefined) queryValues.push(floor-1)
            queryText3 += (floor!==undefined)?" and ad.floor=$"+queryValues.length:""
            dbResponse = await dbQuery.query(queryText3,queryValues)
            action_speak.parameters.text = (intent!=='exist')?dbResponse.rows[0].sum:(dbResponse.rows[0].sum==='0')?confirmation[language_code].no:confirmation[language_code].yes
            app_actions.push(action_speak)
            break;
          case 'distribution':
            intent = action_parameters.intent
            floor = action_parameters.floor
            space = action_parameters.space
            if (space===undefined){
              action_raw_data.parameters.data = await getDistributionFromAccommodation(language_code,parameters.activity_id)
              app_actions.push(action_raw_data)
              continue
            }
            app_actions.splice(app_actions.findIndex((elm => elm.name==='redirect')),1)
            app_actions.splice(app_actions.findIndex((elm => elm.name==='speak')),1)
            queryText3 = "select count(distribution_id) from accommodation_distribution where activity_id=$1 and space=$2 "
            queryValues = [parameters.activity_id,space]
            if (floor!==undefined) queryValues.push(floor-1)
            queryText3 += (floor!==undefined)?"and floor=$3":""
            dbResponse = await dbQuery.query(queryText3,queryValues)
            action_speak.parameters.text = (intent!=='exist')?dbResponse.rows[0].count:(dbResponse.rows[0].count==='0')?confirmation[language_code].no:confirmation[language_code].yes
            app_actions.push(action_speak)
            break;
          case 'places':
            const {latitude,longitude,quantity} = parameters
            const type = action_parameters.place_type + ((action_parameters.included_types!==undefined)?"|"+action_parameters.included_types:"")
            const {data} = await axios.get(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${latitude},${longitude}&rankby=distance&type=${type}&key=${process.env.GOOGLE_API_KEY}&language=${language_code}`)
            action_raw_data.parameters.data = data.results.map(elm => ({location:elm.geometry.location,name:elm.name,icon:elm.icon,type:elm.types[0]}))
            app_actions.push(action_raw_data)
            break;
        }
        break;
      case 'counter':
        queryText3 = "update watson_questions set calls=calls+1 where intent = $1"
        intent = action_parameters.intent + ((action_parameters.entity!==undefined)?"_"+action_parameters.entity:"");
        await dbQuery.query(queryText3,[intent])
        break;
      case 'check':
        switch (action_parameters.type){
          case 'distribution':
            const missing = await checkFieldExists('activity_id','accommodation_distribution','distribution_id',parameters.activity_id)
            if (missing){
              app_actions.splice(app_actions.findIndex((elm => elm.name==='speak')),1)
              app_actions.splice(app_actions.findIndex((elm => elm.name==='redirect')),1)
              app_actions.splice(app_actions.findIndex((elm => elm.name==='query')),1)
              app_actions.push(action_speak)
              app_actions.push(action_gesture)
            }
        }
        break;
    }
  }
  return app_actions
}
*/
const confirmation={
  en:{yes:'Yes, there is',no:'No, there is not'},
  es:{yes:'Sí, se cuenta con ello',no:'No se cuenta con ello'},
  it:{yes:'Sì, si conta',no:'Non si conta'}
}
const actions = {
  'query': {
    'qa': async (app_actions,default_actions,action_parameters,parameters,language_code) => {
      const {activity_id} = parameters
      const {action_speak,action_raw_data,action_gesture} = default_actions
      let queryText3 = "select al.text_value as text,al.language_code,qa.thumb_ref from questions_answers qa "
      queryText3 = addLocalizedQuery(queryText3,1,'qa.qa_id','answer_localized','al','answer')
      queryText3 += " where qa.activity_id=$2 and qa.intent=$3"
      let intent = action_parameters.intent + ((action_parameters.entity!==undefined)?"_"+action_parameters.entity:"");
      const dbResponse = await dbQuery.query(queryText3,[language_code,activity_id,intent])
      if (dbResponse.rows[0]!==undefined){
        action_speak.parameters.text = dbResponse.rows[0].text
        action_speak.parameters.language_code = dbResponse.rows[0].language_code
        action_raw_data.parameters.data = dbResponse.rows[0]
      }else{
        app_actions.splice(app_actions.findIndex((elm => elm.name==='redirect')),1)
        if (app_actions.findIndex((elm => elm.name==='gesture'))!==-1) app_actions.splice(app_actions.findIndex((elm => elm.name==='gesture')),1)
        app_actions.push(action_gesture)
        //await sendInquiryToProvider(parameters.activity_id,intent,client_id)
      }
      app_actions.push(action_speak)
      app_actions.push(action_raw_data)
    },
    'inventory': async (app_actions,default_actions,action_parameters,parameters,language_code)=>{
      const {activity_id} = parameters
      const {action_speak,action_raw_data} = default_actions
      const {intent,item,floor,space} = action_parameters
      if (item===null){
        action_raw_data.parameters.data = await getDistributionFromAccommodation(language_code,activity_id)
        app_actions.push(action_raw_data)
        return
      }
      app_actions.splice(app_actions.findIndex((elm => elm.name==='redirect')),1)
      app_actions.splice(app_actions.findIndex((elm => elm.name==='speak')),1)
      let queryText3 = "select i.item_id,i.thumb_ref,i.quantity from items i inner join accommodation_distribution ad on i.distribution_id=ad.distribution_id where ad.activity_id=$1 and i.entity=$2"
      let queryValues = [activity_id,item]
      if (space!==null) queryValues.push(space)
      queryText3 += (space!==null)?" and ad.space=$"+queryValues.length:""
      if (floor!==null) queryValues.push(floor-1)
      queryText3 += (floor!==null)?" and ad.floor=$"+queryValues.length:""
      const {rows:items} = await dbQuery.query(queryText3,queryValues)
      //const quantity = (dbResponse.rows[0].sum===null)?'0':dbResponse.rows[0].sum
      const quantity = (items.length===0)?0:items.map(elm => elm.quantity).reduce((a, b) => a + b, 0)
      action_speak.parameters.text = (intent!=='exist')?quantity:((quantity===0)?confirmation[language_code].no:confirmation[language_code].yes)
      app_actions.push(action_speak)
    },
    'distribution': async (app_actions,default_actions,action_parameters,parameters,language_code)=>{
      const {activity_id} = parameters
      const {action_speak,action_raw_data} = default_actions
      const {intent,floor,space} = action_parameters
      if (space===null){
        action_raw_data.parameters.data = await getDistributionFromAccommodation(language_code,activity_id)
        app_actions.push(action_raw_data)
        return
      }
      app_actions.splice(app_actions.findIndex((elm => elm.name==='redirect')),1)
      app_actions.splice(app_actions.findIndex((elm => elm.name==='speak')),1)
      let queryText3 = "select distribution_id,thumb_ref from accommodation_distribution where activity_id=$1 and space=$2 "
      let queryValues = [activity_id,space]
      if (floor!==null) queryValues.push(floor-1)
      queryText3 += (floor!==null)?"and floor=$3":""
      const {rows:spaces} = await dbQuery.query(queryText3,queryValues)
      //const quantity = (dbResponse.rows[0].count===null)?'0':dbResponse.rows[0].count
      const quantity = spaces.length
      action_speak.parameters.text = (intent!=='exist')?quantity:((quantity===0)?confirmation[language_code].no:confirmation[language_code].yes)
      app_actions.push(action_speak)
    },
    'places': async (app_actions,default_actions,action_parameters,parameters,language_code)=>{
      const queryText3 = "select * from google_places gp where $1 && gp.types"
      const {action_raw_data} = default_actions
      const {latitude,longitude,radius,quantity} = parameters
      const types = action_parameters.place_type + ((action_parameters.included_types!==undefined)?"|"+action_parameters.included_types:"")
      const {rows:places} = await dbQuery.query(queryText3,[types.split('|')])
      places.map(elm => (elm.distance=getDistance({ latitude: latitude, longitude: longitude },{ latitude: elm.latitude, longitude: elm.longitude })))
      const sorted_places = places.filter(elm => elm.distance<(radius||1000)).sort((a,b)=>{
        return (a.distance-b.distance)
      })
      if (sorted_places.length>100) sorted_places.length = 100
      action_raw_data.parameters.data = sorted_places
      app_actions.push(action_raw_data)
    },
  },
  'counter': async (app_actions,default_actions,action_parameters,parameters,language_code)=>{
    const queryText3 = "update watson_questions set calls=calls+1 where intent = $1"
    const intent = action_parameters.intent + ((action_parameters.entity!==undefined)?"_"+action_parameters.entity:"");
    await dbQuery.query(queryText3,[intent])
  },
  'check': async (app_actions,default_actions,action_parameters,parameters,language_code,back_actions)=>{
    const {activity_id,latitude,longitude,quantity} = parameters
    const {type} = action_parameters
    const {action_speak,action_gesture} = default_actions
    let not_valid;
    switch (type){
      case 'distribution':
        not_valid = ((activity_id===undefined) || await checkFieldExists('activity_id','accommodation_distribution','distribution_id',activity_id))
        break
      case 'qa':
        not_valid = (activity_id===undefined)
        break
      case 'places':
        not_valid = (latitude === undefined || longitude === undefined)
        break
    }
    if (not_valid){
      app_actions.splice(app_actions.findIndex((elm => elm.name==='speak')),1)
      app_actions.splice(app_actions.findIndex((elm => elm.name==='redirect')),1)
      back_actions.splice(back_actions.findIndex((elm => elm.name==='query')),1)
      app_actions.push(action_speak)
      app_actions.push(action_gesture)
    }
  }
}
const askAssistantV2 = async (question,client_id,language_code,parameters) =>{
  const queryText1 = "select conversation_context from client where client_id=$1"
  const queryText2 = "update client set conversation_context=$1 where client_id=$2"
  const missing={
    en:"I couldn't find an answer to your question",
    es:"No pude encontrar una respuesta a tu pregunta",
    it:"Non sono riuscito a trovare una risposta alla tua domanda"
  }
  const default_actions = {
    action_speak : {
      name: "speak",
      parameters: {
        text: missing[language_code],
        language_code:language_code
      }
    },
    action_raw_data : {
      name: "raw_data",
      parameters: {
        data:{}
      }
    },
    action_gesture : {
      name: "gesture",
      parameters: {
        gestures: ["sad"]
      }
    }
  }
  let dbResponse = await dbQuery.query(queryText1,[client_id])
  const conversation_context = dbResponse.rows[0]?dbResponse.rows[0].conversation_context:null
  const {app_actions, back_actions, context} = await consultWatsonV2(language_code,JSON.parse(conversation_context),question)
  await dbQuery.query(queryText2,[JSON.stringify(context),client_id])
  console.log(back_actions)
  let back_action,action_parameters;
  for(let i=0;i<back_actions.length;i++){
    action_parameters = back_actions[i].parameters;
    back_action = (back_actions[i].name==='query')?actions[back_actions[i].name][action_parameters.type]:actions[back_actions[i].name]
    if (back_action) await back_action(app_actions,default_actions,action_parameters,parameters,language_code,back_actions)
  }
  return app_actions
}

const askAssistantTest = async (question,client_id,language_code) =>{
  const queryText1 = "select conversation_context from client where client_id=$1"
  const queryText2 = "update client set conversation_context=$1 where client_id=$2"
  let dbResponse, watsonResponse;

  dbResponse = await dbQuery.query(queryText1,[client_id])
  const conversation_context = dbResponse.rows[0].conversation_context

  watsonResponse = await watsonTest(language_code,JSON.parse(conversation_context),question)
  await dbQuery.query(queryText2,[JSON.stringify(watsonResponse.context),client_id])
  return watsonResponse
}

const login = async (req, res, isClient) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "select * from users where email = $1";
  const queryText2 = "select * from admin where admin_id = $1";
  const queryText3 = "select * from client where client_id = $1";
  const queryText4 = "select prov_id,company_name,provider_status,default_languages,type,coverage from provider where prov_id = $1";
  const queryText5 = "update users set last_access = CURRENT_TIMESTAMP where user_id = $1"
  const queryText6 = "insert into client (client_id,client_status,num_trips) values ($1,$2,$3)";
  let dbResponse;
  try {
    dbResponse = await dbQuery.query(queryText1,[body.email])
    const user = dbResponse.rows[0];
    if (user === undefined){
      eMessage.errors.push({type:'email',msg:'User e-mail does not exist'});
      return res.status(status.conflict).send(eMessage);
    }
    if (!comparePassword(body.password,user.password)){
      eMessage.errors.push({type:'password',msg:'Wrong Password!'});
      return res.status(status.conflict).send(eMessage);
    }
    delete user.password;

    if(isClient){
      dbResponse = await dbQuery.query(queryText3,[user.user_id])
      let client = dbResponse.rows[0];
      if (client === undefined){
        //create client instance
        await dbQuery.query(queryText6,[user.user_id,true,0]);
        client = {client_id:user.user_id,client_status:true,num_trips:0}
        //eMessage.errors.push({type:'user',msg:'This user is not a client!'});
        //return res.status(status.conflict).send(eMessage);
      }
      if (!client.client_status){
        eMessage.errors.push({type:'active',msg:'User is not allowed!'});
        return res.status(status.conflict).send(eMessage);
      }

      sMessage.data.client = client;
    }else{
      dbResponse = await dbQuery.query(queryText2,[user.user_id])
      const admin = dbResponse.rows[0];
      if (admin === undefined){
        eMessage.errors.push({type:'user',msg:'This user is not an admin!'});
        return res.status(status.conflict).send(eMessage);
      }
      if (admin.admin_status !== "active"){
        eMessage.errors.push({type:'active',msg:'User is not allowed yet!'});
        return res.status(status.conflict).send(eMessage);
      }

      dbResponse = await dbQuery.query(queryText4,[admin.provider_id])
      const provider = dbResponse.rows[0];
      if (provider === undefined){
        eMessage.errors.push({type:'exists.provider',msg:'User provider does not exist!'});
        return res.status(status.conflict).send(eMessage);
      }
      if (provider.provider_status !== "active"){
        eMessage.errors.push({type:'active.provider',msg:'User provider is not activated!'});
        return res.status(status.conflict).send(eMessage);
      }
      const languages = provider.default_languages.split(',');
      let default_languages = [];
      languages.forEach(elm => default_languages.push({title: elm.toUpperCase(),  key: elm}))

      const permits = await getPermissionsOfUser(user.user_id)

      sMessage.data.provider = provider;
      sMessage.data.default_languages = default_languages;
      sMessage.data.access = permits;
    }

    await dbQuery.query(queryText5,[user.user_id])
    sMessage.data.user = user;
    sMessage.data.token = generateJWT(user);
    return res.status(status.success).send(sMessage);

  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const saveNearestPlaces = async (latitude,longitude) =>{
  console.log("CONSUMING PLACES!!")
  const queryText = "insert into google_places (place_id, latitude,longitude,name,icon,types) values ($1,$2,$3,$4,$5,$6) on conflict (place_id) do nothing"
  let places = [],place,data,next_page_token;
  const api_key = process.env.GOOGLE_API_KEY;
  const types = ['airport','bakery','bank','beauty_salon','library','church','store','convenience_store','pharmacy','gas_station','hospital','lodging','park','parking','police','restaurant','tourist_attraction'];
  for(let i=0;i<types.length;i++) {
    next_page_token = undefined;
    do{
      (next_page_token)?({data} = await axios.get(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=${data.next_page_token}&key=${api_key}`)):
        ({data} = await axios.get(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${latitude},${longitude}&rankby=distance&type=${types[i]}&key=${api_key}`));
      places = [...places,...data.results.map(elm => ({location:elm.geometry.location,name:elm.name,icon:elm.icon,types:elm.types,place_id:elm.place_id}))];
      ({next_page_token} = data);
    }while(next_page_token)
  }
  console.log(places.length)
  let seen = {},j=0;
  for(let i = 0; i < places.length; i++) {
    place = places[i];
    if(seen[place.place_id] !== 1) {
      j++;
      seen[place.place_id] = 1;
      await dbQuery.query(queryText, [place.place_id, place.location.lat, place.location.lng, place.name, place.icon, place.types])
    }
  }
  console.log(j)
}

module.exports = {
  deleteMultipleByKey,
  deleteMultipleByKeyUnwrapped,
  deleteAllBySingleKey,
  checkFieldExists,
  simpleTableSelect,
  getSingleImageRef,
  checkImageRef,
  saveLocalizedData,
  getLocalizedData,
  updateEntityStatus,
  updateTableSingleField,
  //editLocalizedData,
  addLocalizedQuery,
  getPermissionsOfUser,
  assignAccessGroupsToUser,
  removeAccessGroupsFromUser,
  saveActivitySchedule,
  saveTipsAndRecommendations,
  addGenericService,
  notifyAdminStatusChanged,
  notifyProviderStatusChanged,
  askAssistant,
  askAssistantV2,
  login,
  saveNearestPlaces,

  askAssistantTest,
}