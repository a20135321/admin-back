const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const {hash} = require('../Helpers/encrypt');
const common = require('./common')
const cloneDeep = require('clone-deep');

const EULER_PROVIDER = parseInt(process.env.EULER_PROVIDER)
const OPERATOR_DEFAULT = parseInt(process.env.OPERATOR_DEFAULT)
const PROVIDER_DEFAULT = parseInt(process.env.PROVIDER_DEFAULT)
const ADMIN_DEFAULT = parseInt(process.env.ADMIN_DEFAULT)

const addProviderOrOperator = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = JSON.parse(req.body.data);
  const queryText1 = "insert into provider (company_name,company_email,country,state,city,zipcode,address,type,provider_status,default_languages,coverage) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) returning prov_id";
  const queryText2 = "insert into users (email,password,name,last_name,mobile,avatar_ref) values ($1,$2,$3,$4,$5,$6) returning user_id";
  const queryText3 = "insert into admin (admin_id,provider_id,is_company,admin_status) values ($1,$2,$3,$4)";
  const queryText4 = "insert into client (client_id,client_status,num_trips) values ($1,$2,$3)";
  const queryText5 = "insert into contact_info (prov_id,first_name,last_name,email,mobile_number) values ($1,$2,$3,$4,$5)";
  const queryText6 = "insert into tour_operator_provider (tour_operator_id,prov_id) values ($1,$2)";
  let queryValues,dbResponse,contact;
  try {
    if (!await common.checkFieldExists('email','users','user_id',body.user_email)){
      eMessage.errors.push({type:'exists',msg:'Tour Operator user already exists'});
      return res.status(status.conflict).send(eMessage);
    }
    const country = (body.hasOwnProperty('country'))?body.country.label:"Undef";
    const state = (body.hasOwnProperty('state'))?body.state.label:"Undef";
    const city = (body.hasOwnProperty('city'))?body.city.label:"Undef";
    const type = (body.hasOwnProperty('type'))?body.type.toLowerCase():"operator";
    const parentID = (body.hasOwnProperty('operator'))?body.operator.value:null;
    const default_languages = body.languages.map(elm => elm.key).join()
    const coverage = body.coverage.map(elm => (elm.country+'/'+elm.state) ).join()

    queryValues = [body.company_name,body.company_email,country,state,city,body.zipcode,body.address,type,'pending',default_languages,coverage];
    dbResponse = await dbQuery.query(queryText1,queryValues);
    const provider_id = dbResponse.rows[0].prov_id;
    await common.saveLocalizedData(body.languages,body,'provider_localized','description',provider_id)

    const [avatar_ref,] = common.getSingleImageRef('user',req.files,'avatar')
    const hashed_password = await hash(body.password);
    queryValues = [body.user_email,hashed_password,body.first_name,body.last_name,body.mobile,avatar_ref];
    dbResponse = await dbQuery.query(queryText2,queryValues);
    const user_id = dbResponse.rows[0].user_id;

    queryValues = [user_id,provider_id,true,'active'];
    await dbQuery.query(queryText3,queryValues);
    queryValues = [user_id,true,0];
    await dbQuery.query(queryText4,queryValues);

    const permission_groups = (body.hasOwnProperty('permission_groups'))?body.permission_groups:[OPERATOR_DEFAULT]
    if (!permission_groups.includes(PROVIDER_DEFAULT)){
      permission_groups.push(PROVIDER_DEFAULT)
      permission_groups.push(ADMIN_DEFAULT)
    }
    await common.assignAccessGroupsToUser(user_id,permission_groups)

    for(let i=0;i<body.contacts.length;i++){
      contact = body.contacts[i];
      queryValues = [provider_id,contact.first_name,contact.last_name,contact.email,contact.mobile];
      await dbQuery.query(queryText5,queryValues);
    }

    if (parentID!==null) {
      await dbQuery.query(queryText6,[parentID,provider_id]);
    }

    return res.status(status.created).send(sMessage);

  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const editProviderOrOperator = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "update provider set company_name=$1,type=$2,country=$3,state=$4,city=$5,zipcode=$6,address=$7,provider_status=$8,default_languages=$9,coverage=$10 where prov_id=$11";
  const queryText2 = "insert into contact_info (prov_id,first_name,last_name,email,mobile_number) values ($1,$2,$3,$4,$5)";
  let queryValues,contact;
  try {
    const provider_id = body.prov_id;
    const country = (body.hasOwnProperty('country'))?body.country:"Undef";
    const state = (body.hasOwnProperty('state'))?body.state:"Undef";
    const city = (body.hasOwnProperty('city'))?body.city:"Undef";
    const type = (body.hasOwnProperty('type'))?body.type.toLowerCase():"operator";
    const default_languages = body.languages.map(elm => elm.key).join();
    const coverage = body.coverage.map(elm => (elm.country+'/'+elm.state) ).join()
    console.log(coverage)
    await common.notifyProviderStatusChanged(provider_id,body.provider_status)
    queryValues = [body.company_name,type,country,state,city,body.zipcode,body.address,body.provider_status,default_languages,coverage,provider_id];
    await dbQuery.query(queryText1,queryValues);

    await common.saveLocalizedData(body.languages,body,'provider_localized','description',provider_id)

    await common.deleteAllBySingleKey('contact_info','prov_id',provider_id)
    for(let i=0;i<body.contacts.length;i++){
      contact = body.contacts[i];
      queryValues = [provider_id,contact.first_name,contact.last_name,contact.email,contact.mobile_number];
      await dbQuery.query(queryText2,queryValues);
    }

    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const deleteProvidersByID = async (req, res) =>{
  return await common.deleteMultipleByKey(req,res,'provider','prov_id',[EULER_PROVIDER])
}

const getSelectorProviders = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText = "select p.company_name,p.prov_id from provider p";
  let queryValues = []
  try {
    const look_for = body.look_for;
    if (look_for == 'mine'){
      queryValues.push(body.myID);
      queryText += " inner join tour_operator_provider top on p.prov_id=top.prov_id where top.tour_operator_id=$1 and";
    }else if(look_for == 'operator'){
      queryText += " where p.type='operator' and"
    }else{
      queryText += " where"
    }
    queryValues.push('%'+body.value+'%')
    queryText += " p.company_name like $"+queryValues.length;

    const dbResponse = await dbQuery.query(queryText,queryValues);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getMyProviders = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText1 = "select p.prov_id, p.provider_status,p.address, p.company_name, p.city||'/'||p.country as city_country, p.type, dl.text_value as description from provider p";
  queryText1 = common.addLocalizedQuery(queryText1,1,'p.prov_id','provider_localized','dl','description');
  let queryValues = [body.language_code], dbResponse;
  const filter = body.hasOwnProperty('filter')?body.filter.toLowerCase():'all';
  try {
    if (body.hasOwnProperty('myID')){
      queryValues.push(body.myID);
      queryText1 += " inner join tour_operator_provider top on top.prov_id=p.prov_id where top.tour_operator_id=$"+queryValues.length;
    }
    if (filter !== 'all'){
      (filter!=='provider_status')?queryValues.push('%'+body.value+'%'):queryValues.push(body.value)
      queryText1 += body.hasOwnProperty('myID')?" and":" where";
      queryText1 += " p."+filter+" like $"+queryValues.length;
    }
    queryText1 += " order by p.company_name asc"
    dbResponse = await dbQuery.query(queryText1,queryValues);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }

    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  }catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const getProviderEditableDetails = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText1 = "select * from provider where prov_id=$1";
  const queryText2 = "select p.company_name from tour_operator_provider top inner join provider p on top.tour_operator_id=p.prov_id where top.prov_id=$1"
  const queryText3 = "select * from contact_info where prov_id=$1"
  let provider, dbResponse, operator,languages,default_languages,coverage=[];
  try {
    const provider_id = body.provider_id;
    dbResponse = await dbQuery.query(queryText1,[provider_id]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    provider = dbResponse.rows[0]

    if (provider.coverage!==null){
      provider.coverage.split(',').forEach(elm => {
        coverage.push({country:elm.split('/')[0],state:elm.split('/')[1]})
      })
    }
    provider.coverage = coverage

    dbResponse = await dbQuery.query(queryText2,[provider_id])
    operator = (dbResponse.rows[0]===undefined)?null:dbResponse.rows[0].company_name;
    provider.operator=operator;

    dbResponse = await dbQuery.query(queryText3,[provider_id])
    provider.contacts=dbResponse.rows;

    languages = provider.default_languages.split(',');
    default_languages = [];
    languages.forEach(elm => default_languages.push({title: elm.toUpperCase(),  key: elm}))
    provider.default_languages=default_languages;

    dbResponse = await common.getLocalizedData('provider_localized','description',provider_id)
    if (dbResponse.rows[0] !== undefined) {
      dbResponse.rows.forEach(elm => {
        provider['description'+'-'+elm.language_code] = elm.text_value;
      })
    }

    sMessage.data = provider;
    return res.status(status.success).send(sMessage);
  }catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const updateProviderStatus = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  if (req.body.data.id!==EULER_PROVIDER){
    return await common.updateEntityStatus(req,res,'provider','prov_id')
  }else{
    eMessage.errors.push({type:'undeletable',msg:'Can not change this!'});
    return res.status(status.conflict).send(eMessage);
  }
}

const updateProviderLanguages = async (req, res) =>{
  return await common.updateTableSingleField(req,res,'provider','default_languages','prov_id')
}

module.exports = {
  addProviderOrOperator,
  editProviderOrOperator,
  getSelectorProviders,
  getMyProviders,
  updateProviderStatus,
  deleteProvidersByID,
  updateProviderLanguages,
  getProviderEditableDetails,
}