const moment  = require('moment')
const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const common = require('./common')
const cloneDeep = require('clone-deep');

const EULER_ADMIN = parseInt(process.env.EULER_ADMIN)
const EULER_GROUP = parseInt(process.env.EULER_GROUP)
const OPERATOR_DEFAULT = parseInt(process.env.OPERATOR_DEFAULT)
const ADMIN_DEFAULT = parseInt(process.env.ADMIN_DEFAULT)
const PROVIDER_DEFAULT = parseInt(process.env.PROVIDER_DEFAULT)

const getGroups = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const queryText1 = "select * from access_groups order by group_name asc"
  const queryText2 = "select * from permissions order by permission_name asc"
  let dbResponse;
  try {
    dbResponse = await dbQuery.query(queryText1,[]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'groups',msg:'No access groups found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data.groups=dbResponse.rows;

    dbResponse = await dbQuery.query(queryText2,[]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'permissions',msg:'No permissions found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data.permissions=dbResponse.rows;

    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const getPermissionsInGroup = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "select * from permissions"
  const queryText2 = "select permission_id from group_permissions where group_id=$1"
  let dbResponse,active_permits=[];
  try {
    dbResponse = await dbQuery.query(queryText1,[]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'permissions',msg:'No permissions found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    const permissions = dbResponse.rows;

    dbResponse = await dbQuery.query(queryText2,[body.group_id]);
    const group_permissions = (dbResponse.rows[0] === undefined)?[]:dbResponse.rows;
    group_permissions.forEach(elm => active_permits.push(elm.permission_id))

    for(let i=0;i<permissions.length;i++){
      if (active_permits.includes(permissions[i].permission_id)){
        permissions[i].status = 'enabled'
      }else{
        permissions[i].status = 'disabled'
      }
    }

    return res.status(status.success).send(permissions);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const addAccessGroup = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "insert into access_groups (group_name,enabled) values ($1,$2) returning group_id"
  try{
    if (await common.checkFieldExists('group_name','access_groups','group_id',body.group_name)){
      const dbResponse = await dbQuery.query(queryText,[body.group_name,true]);

      await common.assignAccessGroupsToUser(EULER_ADMIN,[dbResponse.rows[0].group_id])

      return res.status(status.success).send(sMessage);
    }else{
      eMessage.errors.push({type:'exists',msg:'This access group already exists!'});
      return res.status(status.conflict).send(eMessage);
    }
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const deleteAccessGroup = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  if ([EULER_GROUP,ADMIN_DEFAULT,OPERATOR_DEFAULT,PROVIDER_DEFAULT].includes(req.body.data.group_id)){
    eMessage.errors.push({type:'undeletable',msg:'Cannot delete this permission group!'});
    return res.status(status.conflict).send(eMessage);
  }else{
    return await common.deleteMultipleByKey(req,res,'access_groups','group_id')
  }
}

const addPermission = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "insert into permissions (permission_name) values ($1) returning permission_id"
  const queryText2 = "insert into group_permissions (group_id,permission_id) values ($1,$2)"
  try{
    if (await common.checkFieldExists('permission_name','permissions','permission_id',body.permission_name)){
      const dbResponse = await dbQuery.query(queryText1,[body.permission_name]);
      const permission_id = dbResponse.rows[0].permission_id;
      await dbQuery.query(queryText2,[EULER,permission_id]);

      return res.status(status.success).send(sMessage);
    }else{
      eMessage.errors.push({type:'exists',msg:'This permission already exists!'});
      return res.status(status.conflict).send(eMessage);
    }
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const deletePermissions= async (req, res) =>{
  return await common.deleteMultipleByKey(req,res,'permissions','permission_id')
}

const updatePermissionInGroup = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "insert into group_permissions (group_id,permission_id) values ($1,$2)"
  const queryText2 = "delete from group_permissions where group_id=$1 and permission_id=$2"
  try{
    if (body.group_id!==EULER_GROUP){
      const new_status = body.new_status;
      const queryValues = [body.group_id,body.permission_id]
      if (new_status==='enabled'){
        await dbQuery.query(queryText1,queryValues);
      }else if(new_status==='disabled'){
        await dbQuery.query(queryText2,queryValues);
      }
      return res.status(status.success).send(sMessage);
    }else{
      eMessage.errors.push({type:'undeletable',msg:"Can not modify this group!"});
      return res.status(status.conflict).send(eMessage);
    }
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const updateAccessGroupStatus = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "update access_groups set enabled=$1 where group_id=$2"
  try{
    if (body.group_id!==EULER_GROUP){
      const new_status = body.new_status;
      if (new_status==='enabled'){
        await dbQuery.query(queryText,[true,body.group_id]);
      }else if(new_status==='disabled'){
        await dbQuery.query(queryText,[false,body.group_id]);
      }
      return res.status(status.success).send(sMessage);
    }else{
      eMessage.errors.push({type:'undeletable',msg:"Can not change this group state!"});
      return res.status(status.conflict).send(eMessage);
    }
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const getGroupsOfUser = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  try{
    const permitsAndGroups = await common.getPermissionsOfUser(body.user_id)
    sMessage.data.groups = permitsAndGroups.groups;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const updateGroupsOfUser = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  try{
    if (body.user_id!==EULER_ADMIN){
      const groupsToUpdate = body.groups;
      const toRemove = groupsToUpdate.map(elm => elm.group_id);
      const toAdd = groupsToUpdate.filter(elm => elm.enabled).map(elm => elm.group_id);

      await common.removeAccessGroupsFromUser(body.user_id,toRemove)
      await common.assignAccessGroupsToUser(body.user_id,toAdd)
      return res.status(status.success).send(sMessage);
    }else{
      eMessage.errors.push({type:'undeletable',msg:"Can not modify this!"});
      return res.status(status.conflict).send(eMessage);
    }
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

module.exports = {
  getGroups,
  getGroupsOfUser,
  updateGroupsOfUser,
  addAccessGroup,
  addPermission,
  deleteAccessGroup,
  deletePermissions,
  updatePermissionInGroup,
  updateAccessGroupStatus,
  getPermissionsInGroup,
}