const moment  = require('moment')
const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const common = require('./common')
const cloneDeep = require('clone-deep');

//missing app side filtering
const getToursByFilter = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  let queryText = "select a.* , t.* , u.name ||' ' || u.last_name as provider from activity a inner join tour t on a.activity_id = t.tour_id inner " +
    "join user u on a.prov_id = u.user_id";
  let queryValues;

  try {
    if (body.hasOwnProperty('ids')){
      queryText = queryText + " where u.user_id = ANY($1)";
      queryValues = [body.ids];
    }

    if (body.hasOwnProperty('filter')){
      const filter = body.filter;
      if (filter !== 'all'){
        queryValues = [body.ids,'%'+body.value+'%'];
        if (filter==='provider'){
          queryText = queryText + " and u.name like $2 or u.last_name like $2";
        }else if(filter==="title"){
          queryText = queryText + " and a.title like $2";
        }
      }
    }

    const {rows} = await dbQuery.query(queryText,queryValues);
    const dbResponse = rows;
    if (dbResponse[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const getToursInDateRange = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const start_date = req.body.data.start_date;
  const end_date = req.body.data.end_date;

  const queryText1 = "select * from activity_schedule where date between $1 and $2";
  const queryText2 = "select activity_id,title,description,image,frequency,duration from activity where activity_id = ANY($1) and category='tour'";
  let dbResponse,rows;
  try {
    dbResponse = await dbQuery.query(queryText1,[start_date,end_date]);
    rows = dbResponse.rows;
    if (rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data.schedule = rows;

    let activities = [];
    for(let i=0;i<rows.length;i++){
      activities.push(rows[i].activity_id)
    }
    activities = [...new Set(activities)]

    dbResponse = await dbQuery.query(queryText2,[activities]);
    rows = dbResponse.rows;
    sMessage.data.tours = (rows === undefined)?[]:rows;

    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const getTourInfoByID = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const id = body.tour_id;
  let queryText1 = "select a.*,dl.text_value as description, sdl.text_value as short_description from activity a inner join tour t on a.activity_id = t.tour_id";
  queryText1 = common.addLocalizedQuery(queryText1,1,'a.activity_id','activity_localized','sdl','activity_short_description')
  queryText1 = common.addLocalizedQuery(queryText1,1,'a.activity_id','activity_localized','dl','activity_description')
  queryText1 += " where t.tour_id = $2"
  const queryText2 = "select entry from itinerary where tour_id = $1";
  let dbResponse,rows;
  try {
    dbResponse = await dbQuery.query(queryText1,[body.language_code,id]);
    rows = dbResponse.rows;
    const tour = rows[0];
    if (tour === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data.details = tour;

    dbResponse = await dbQuery.query(queryText2,[id]);
    rows = dbResponse.rows;
    sMessage.data.itinerary = (rows[0] === undefined)?[]:rows;

    const tour_type = tour.is_guided?'':'self_';
    let queryText3 = "select ml.text_value from tip t inner join activity a on a.activity_id=t.associated_id"
    queryText3 = common.addLocalizedQuery(queryText3,1,'t.tip_id','tip_localized','ml','message')
    queryText3 += " where a.activity_id=$2 and t.associated_entity='activity'"

    //queryText3 += " where t.associated_id in (select ts.place_of_interest_id from tour_stop ts inner join "+tour_type+"guided_tour_stop gts on " +
    //  "ts.tour_stop_id=gts."+tour_type+"guided_tour_stop_id where gts."+tour_type+"guided_tour_id = $2) and t.associated_entity='POI'";

    //console.log(queryText3)
    dbResponse = await dbQuery.query(queryText3,[body.language_code,id]);
    rows = dbResponse.rows;
    sMessage.data.tips = (rows[0] === undefined)?[]:rows;

    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}


const addTour = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = JSON.parse(req.body.data);
  const queryText1 = "insert into activity (prov_id,title,latitude,longitude,website,duration,cost,max_persons,activity_status,frequency,category,visit_start_time,visit_end_time) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13) returning activity_id";
  const queryText2 = "insert into activity_images (activity_id,image_ref,thumb_ref) values ($1,$2,$3)"
  const queryText3 = "insert into tour (tour_id,visits_per_year,is_guided) values ($1,$2,$3)";
  const queryText4 = "insert into itinerary (entry,tour_id) values ($1,$2)";
  let dbResponse,image_ref,thumb_ref;
  const activity_status = (body.activity_status)?'active':'pending';
  let queryValues = [body.provider.value,body.title,body.itineraries[0].poi.value[1],body.itineraries[0].poi.value[2],
    body.website,body.duration,body.cost,body.max_persons,activity_status,body.frequency,'tour',moment(body.visiting_hours[0]).format('HH:mm:ss'),moment(body.visiting_hours[1]).format('HH:mm:ss')];

  try {
    //save in activity table
    dbResponse = await dbQuery.query(queryText1,queryValues);
    const id = dbResponse.rows[0].activity_id;
    await common.saveLocalizedData(body.languages,body,'activity_localized','activity_description',id);
    await common.saveLocalizedData(body.languages,body,'activity_localized','activity_short_description',id);

    for(let i=0;i<body.total_images;i++){
      [image_ref,thumb_ref] = common.getSingleImageRef('service',req.files,'activity'+i)
      queryValues = [id,image_ref,thumb_ref]
      await dbQuery.query(queryText2,queryValues);
    }

    //save in tour table
    const is_guided = (body.tour_type==='Guided')?true:false;
    queryValues = [id,body.visits_per_year,is_guided]
    await dbQuery.query(queryText3,queryValues);
    //save itineraries
    for(let i=0;i<body.activities.length;i++){
      queryValues = [body.activities[i].entry,id]
      await dbQuery.query(queryText4,queryValues);
    }
    //save in (self)guided_tour
    const header = is_guided?'':'self_';
    const queryText5 = "insert into "+header+"guided_tour ("+header+"guided_tour_id) values ($1);";
    await dbQuery.query(queryText5,[id]);

    //save in tour stop & (self)guided_tour_stop
    const queryText6 = "insert into tour_stop (place_of_interest_id,image) values ($1,$2) returning tour_stop_id";
    const ts_fields = is_guided?",duration_of_stop,stop_number":"";
    const ts_values = is_guided?",$3,$4":"";
    const queryText7 = "insert into "+header+"guided_tour_stop ("+header+"guided_tour_stop_id,"+header+"guided_tour_id"+ts_fields+") values ($1,$2"+ts_values+")";

    let POI,ts_id;
    for(let i=0;i<body.itineraries.length;i++){
      POI = body.itineraries[i];
      [image_ref,] = common.getSingleImageRef('tour_stop',req.files,'ts'+i);
      queryValues = [POI.poi.value[0],image_ref]
      dbResponse = await dbQuery.query(queryText6,queryValues);
      ts_id = dbResponse.rows[0].tour_stop_id;
      await common.saveLocalizedData(body.languages,POI,'activity_localized','ts_history',ts_id)
      await common.saveLocalizedData(body.languages,POI,'activity_localized','ts_short_description',ts_id)

      queryValues = is_guided?[ts_id,id,POI.duration_of_stop,i]:[ts_id,id]
      await dbQuery.query(queryText7,queryValues);
    }
    //save in schedule
    await common.saveActivitySchedule(body,id)
    return res.status(status.created).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getTourEditableDetails = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "select * from activity where activity_id=$1";
  const queryText2 = "select entry from itinerary where tour_id=$1";
  let dbResponse,languages = [],activity;
  try {
    const activity_id = body.activity_id;
    dbResponse = await dbQuery.query(queryText1,[activity_id]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    activity = dbResponse.rows[0];

    dbResponse = await common.getLocalizedData('activity_localized','activity_description',activity_id)
    if (dbResponse.rows[0] !== undefined) {
      dbResponse.rows.forEach(elm => {
        languages.push(elm.language_code)
        activity['activity_description'+'-'+elm.language_code] = elm.text_value;
      })
    }

    dbResponse = await common.getLocalizedData('activity_localized','activity_short_description',activity_id)
    if (dbResponse.rows[0] !== undefined) {
      dbResponse.rows.forEach(elm => {
        languages.push(elm.language_code)
        activity['activity_short_description'+'-'+elm.language_code] = elm.text_value;
      })
    }

    let default_languages = [];
    languages.filter((v,i,a)=>a.indexOf(v)===i).forEach(elm => default_languages.push({title: elm.toUpperCase(),  key: elm}))
    activity.default_languages = default_languages;

    dbResponse = await dbQuery.query(queryText2,[activity_id]);
    activity.activities = (dbResponse.rows[0]===undefined)?[]:dbResponse.rows;


    sMessage.data = activity;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

module.exports = {
  getToursByFilter,
  getToursInDateRange,
  getTourInfoByID,
  addTour,
  getTourEditableDetails,
}