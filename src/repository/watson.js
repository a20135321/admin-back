const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const cloneDeep = require('clone-deep');
const common = require('./common')


const getWatsonQuestions = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText = "select * from watson_questions"
  let queryValues=[];
  try {
    if (body.hasOwnProperty('intent')){
      queryValues.push('%'+body.intent.toLowerCase()+'%')
      queryText += " where lower(intent) like $1"
    }
    queryText += " order by intent asc"
    const dbResponse = await dbQuery.query(queryText,queryValues)
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }

    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getWatsonQuestionsSelector = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText = "select intent,"+body.language_code+" as text from watson_questions where faq"
  try {
    const dbResponse = await dbQuery.query(queryText,[])
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }

    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const upsertQuestion = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "update questions_answers set intent=$1 from watson_questions wq where wq.question_id=$2 and wq.intent=questions_answers.intent"
  const queryText2 = "insert into watson_questions (en,es,it,intent,calls,faq) values ($1,$2,$3,$4,$5,$6)"
  try {
    await dbQuery.query("BEGIN")
    if (body.hasOwnProperty('question_id')){
      await dbQuery.query(queryText1,[body.intent,body.question_id]);
      await common.deleteAllBySingleKey('watson_questions','question_id',body.question_id)
    }
    if (!await common.checkFieldExists('intent','watson_questions','question_id',body.intent)){
      await dbQuery.query("ROLLBACK")
      eMessage.errors.push({type:'exists',msg:'Intent already exists!'});
      return res.status(status.error).send(eMessage);
    }
    const calls = (body.hasOwnProperty('calls'))?body.calls:0
    await dbQuery.query(queryText2,[body.en,body.es,body.it,body.intent,calls,body.faq]);
    await dbQuery.query("COMMIT")
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    await dbQuery.query("ROLLBACK")
    return res.status(status.error).send(eMessage);
  }
}


const deleteQuestions = async (req, res) =>{
  await common.deleteMultipleByKey(req,res,'watson_questions','question_id',[],'questions_answers','intent','intent')
}


module.exports = {
  getWatsonQuestions,
  getWatsonQuestionsSelector,
  upsertQuestion,
  deleteQuestions,
}


