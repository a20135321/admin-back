const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const common = require('./common')
const cloneDeep = require('clone-deep');

const addPOI = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "insert into place_of_interest (prov_id,category,name,country,city,address,longitude,latitude) values " +
    "($1,$2,$3,$4,$5,$6,$7,$8) returning place_of_interest_id";
  let dbResponse,queryValues;
  try {
    const country = (body.hasOwnProperty('country'))?body.country.label:"Undef";
    const city = (body.hasOwnProperty('city'))?body.city.label:"Undef";
    queryValues = [body.provider.value,body.category,body.name,country,city,body.address,body.longitude,body.latitude];
    dbResponse = await dbQuery.query(queryText1,queryValues);
    const poi_id = dbResponse.rows[0].place_of_interest_id;

    await common.saveLocalizedData(body.languages,body,'poi_localized','description',poi_id);
    await common.saveLocalizedData(body.languages,body,'poi_localized','short_description',poi_id);

    await common.saveTipsAndRecommendations(body,"POI",poi_id)

    return res.status(status.created).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const getPOIEditableDetails = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText1 = "select poi.*, p.company_name from place_of_interest poi inner join provider p on p.prov_id=poi.prov_id where poi.place_of_interest_id=$1";
  let queryText2 = "select * from tip where associated_entity='POI' and associated_id=$1";

  let dbResponse,languages = [],poi,tips;
  try {
    const poi_id = body.poi_id
    dbResponse = await dbQuery.query(queryText1,[poi_id]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    poi = dbResponse.rows[0];

    dbResponse = await common.getLocalizedData('poi_localized','description',poi_id)
    if (dbResponse.rows[0] !== undefined) {
      dbResponse.rows.forEach(elm => {
        languages.push(elm.language_code)
        poi['description'+'-'+elm.language_code] = elm.text_value;
      })
    }

    dbResponse = await common.getLocalizedData('poi_localized','short_description',poi_id)
    if (dbResponse.rows[0] !== undefined) {
      dbResponse.rows.forEach(elm => {
        languages.push(elm.language_code)
        poi['short_description'+'-'+elm.language_code] = elm.text_value;
      })
    }

    let default_languages = [];
    languages.filter((v,i,a)=>a.indexOf(v)===i).forEach(elm => default_languages.push({title: elm.toUpperCase(),  key: elm}))
    poi.default_languages = default_languages;

    dbResponse = await dbQuery.query(queryText2,[poi_id]);
    tips = dbResponse.rows;
    for(let i=0;i<tips.length;i++){
      dbResponse = await common.getLocalizedData('tip_localized','message',tips[i].tip_id)
      if (dbResponse.rows[0] !== undefined) {
        dbResponse.rows.forEach(elm => {
          tips[i]['message'+'-'+elm.language_code] = elm.text_value;
        })
      }

      dbResponse = await common.getLocalizedData('tip_localized','title',tips[i].tip_id)
      if (dbResponse.rows[0] !== undefined) {
        dbResponse.rows.forEach(elm => {
          tips[i]['title'+'-'+elm.language_code] = elm.text_value;
        })
      }
    }

    poi.tips = tips.filter(elm => elm.is_tip)
    poi.recoms = tips.filter(elm => !elm.is_tip)

    sMessage.data = poi;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}


const deletePOIsByID = async (req, res) =>{
  return await common.deleteMultipleByKey(req,res,'place_of_interest','place_of_interest_id');
}

const getPOIsByFilter = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText = "select p.company_name, poi.place_of_interest_id, poi.name,poi.latitude,poi.longitude, poi.category, poi.country, poi.city, sdl.text_value as short_description, poi.address from place_of_interest poi" +
    " inner join provider p on p.prov_id=poi.prov_id ";
  queryText = common.addLocalizedQuery(queryText,1,'poi.place_of_interest_id','poi_localized','sdl','short_description')
  let queryValues=[],dbResponse;

  try {
    queryValues.push(body.language_code);
    if (body.look_for == 'mine'){
      queryValues.push(body.myID);
      queryText += " left join tour_operator_provider top on p.prov_id=top.prov_id where (top.tour_operator_id=$"+queryValues.length+" or p.prov_id=$"+queryValues.length+")";
    }
    const filter = (body.hasOwnProperty('filter'))?body.filter:'all';
    if (filter !== 'all'){
      queryText += (queryValues.length===2)?" and":" where"
      queryValues.push('%'+body.value+'%')
      queryText += (filter==='company_name')?" p."+filter+" like $"+queryValues.length:" poi."+filter+" like $"+queryValues.length;
    }
    queryText += " order by poi.name asc"

    dbResponse = await dbQuery.query(queryText,queryValues);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }

    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    return res.status(status.error).send(eMessage);
  }
}

const editPOI = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  console.log(body)
  const queryText1 = "update place_of_interest set category=$1,name=$2,country=$3,city=$4,address=$5,longitude=$6,latitude=$7 where place_of_interest_id=$8"
  const queryText2 = "delete from tip where associated_entity='POI' and associated_id=$1"
  let queryValues;
  try {
    const poi_id = body.place_of_interest_id;
    const country = (body.hasOwnProperty('country'))?body.country:"Undef";
    const city = (body.hasOwnProperty('city'))?body.city:"Undef";
    queryValues = [body.category,body.name,country,city,body.address,body.longitude,body.latitude,poi_id];
    await dbQuery.query(queryText1,queryValues);

    await common.saveLocalizedData(body.languages,body,'poi_localized','description',poi_id)
    await common.saveLocalizedData(body.languages,body,'poi_localized','short_description',poi_id);

    await dbQuery.query(queryText2,[poi_id]);
    await common.saveTipsAndRecommendations(body,"POI",poi_id)

    return res.status(status.created).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getSelectorPOI = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText = "select p.company_name || ':' ||poi.name as name, poi.place_of_interest_id, poi.latitude, poi.longitude from place_of_interest poi inner join provider p" +
    " on p.prov_id=poi.prov_id left join tour_operator_provider top on top.tour_operator_id=p.prov_id where" +
    " (top.prov_id=$1 or poi.prov_id=$1) order by poi.name asc";
  try {
    const dbResponse = await dbQuery.query(queryText,[body.prov_id]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

module.exports = {
  addPOI,
  getPOIEditableDetails,
  deletePOIsByID,
  getPOIsByFilter,
  editPOI,
  getSelectorPOI,
}