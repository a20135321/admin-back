const moment  = require('moment')
const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const common = require('./common')
const cloneDeep = require('clone-deep');

const addAccommodationService = async (req, res) =>{
  return await common.addGenericService(req,res,'accommodation')
}

const addRestaurantService = async (req, res) =>{
  return await common.addGenericService(req,res,'restaurant')
}

const addOtherService = async (req, res) =>{
  return await common.addGenericService(req,res,undefined)
}

const getAccommodationsInDateRange = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;

  let queryText1 = "select a.*, dl.text_value as desc, sdl.text_value as short_desc from activity a "
  queryText1 = common.addLocalizedQuery(queryText1,1,'a.activity_id','activity_localized','dl','activity_description')
  queryText1 = common.addLocalizedQuery(queryText1,1,'a.activity_id','activity_localized','sdl','activity_short_description')
  queryText1 += " inner join (select activity_id, count(activity_id) from (select distinct activity_id,date from activity_schedule where date between $2 and $3) as as2" +
    " group by activity_id having count(activity_id)=$4) as aa on a.activity_id=aa.activity_id where a.category='accommodation'";
  const queryText2 = "select activity_id,image_ref from activity_images where activity_id = ANY($1) ";
  const queryText3 = "select * from activity_schedule where activity_id = ANY($1) and date between $2 and $3";
  let dbResponse,rows;
  try {
    const start_date = body.start_date;
    const end_date = body.end_date;
    const language_code = body.language_code;
    const number_days = moment(end_date).diff(moment(start_date),'days')+1;

    dbResponse = await dbQuery.query(queryText1,[language_code,start_date,end_date,number_days]);
    rows = dbResponse.rows;
    if (rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    const accommodations = rows;

    const activities = accommodations.map(elm => elm.activity_id)
    dbResponse = await dbQuery.query(queryText2,[activities]);
    rows = dbResponse.rows;
    const images = (rows[0] === undefined)?[]:rows;
    for(let i=0;i<accommodations.length;i++){
      accommodations[i].images = images.filter(image => image.activity_id===accommodations[i].activity_id).map(elm => elm.image_ref)
    }
    sMessage.data.accommodations = accommodations;

    dbResponse = await dbQuery.query(queryText3,[activities,start_date,end_date]);
    rows = dbResponse.rows;
    const schedule = (rows[0] === undefined)?[]:rows;
    sMessage.data.schedule = schedule;

    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getServicesByFilter = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText = "select a.activity_id,a.title,p.company_name,a.category,a.cost,a.activity_status, al.text_value as short_description from activity a inner join provider p on a.prov_id=p.prov_id";
  queryText = common.addLocalizedQuery(queryText,1,'a.activity_id','activity_localized','al','activity_short_description')
  let queryValues = [],dbResponse;
  try {
    queryValues.push(body.language_code);
    if (body.look_for == 'mine'){
      queryValues.push(body.myID);
      queryText += " left join tour_operator_provider top on p.prov_id=top.prov_id where (top.tour_operator_id=$"+queryValues.length+" or p.prov_id=$"+queryValues.length+")";
    }
    const filter = (body.hasOwnProperty('filter'))?body.filter:'all';
    if (filter !== 'all'){
      queryText += (queryValues.length===2)?" and":" where"
      queryValues.push('%'+body.value.toLowerCase()+'%')
      queryText += (filter==='company_name')?" lower(p."+filter+") like $"+queryValues.length:" lower(a."+filter+") like $"+queryValues.length;
    }
    queryText += " order by a.title asc"
    dbResponse = await dbQuery.query(queryText,queryValues);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }

    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getServiceEditableDetails = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "select * from activity where activity_id=$1";
  const queryText2 = "select * from tip where associated_entity='activity' and associated_id=$1";
  const queryText3 = "select * from questions_answers where activity_id=$1";
  const queryText4 = "select image_ref from activity_images where activity_id = $1"
  const queryText5 = "select calendar from schedule_brief where activity_id = $1"

  //const queryText6 = "select ad.floor,ad.space,ad.image_ref as space_image,ad.image_ref as space_thumb,i.*" + "from accommodation_distribution ad inner join items i on ad.distribution_id=i.distribution_id where activity_id=$1"

  const queryText6 = "select * from accommodation_distribution where activity_id=$1"
  const queryText7 = "select * from items i where distribution_id=$1"

  let dbResponse,languages = [],activity,tips,qas;
  try {
    const activity_id = body.activity_id;
    dbResponse = await dbQuery.query(queryText1,[activity_id]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    activity = dbResponse.rows[0];

    dbResponse = await common.getLocalizedData('activity_localized','activity_description',activity_id)
    if (dbResponse.rows[0] !== undefined) {
      dbResponse.rows.forEach(elm => {
        languages.push(elm.language_code)
        activity['activity_description'+'-'+elm.language_code] = elm.text_value;
      })
    }

    dbResponse = await common.getLocalizedData('activity_localized','activity_short_description',activity_id)
    if (dbResponse.rows[0] !== undefined) {
      dbResponse.rows.forEach(elm => {
        languages.push(elm.language_code)
        activity['activity_short_description'+'-'+elm.language_code] = elm.text_value;
      })
    }

    dbResponse = await dbQuery.query(queryText4,[activity_id]);
    activity.images = dbResponse.rows.map(elm => elm.image_ref)

    dbResponse = await dbQuery.query(queryText5,[activity_id]);
    activity.calendar = dbResponse.rows.map(elm => elm.calendar)

    dbResponse = await dbQuery.query(queryText2,[activity_id]);
    tips = dbResponse.rows;
    for(let i=0;i<tips.length;i++){
      dbResponse = await common.getLocalizedData('tip_localized','message',tips[i].tip_id)
      if (dbResponse.rows[0] !== undefined) {
        dbResponse.rows.forEach(elm => {
          tips[i]['message'+'-'+elm.language_code] = elm.text_value;
        })
      }

      dbResponse = await common.getLocalizedData('tip_localized','title',tips[i].tip_id)
      if (dbResponse.rows[0] !== undefined) {
        dbResponse.rows.forEach(elm => {
          tips[i]['title'+'-'+elm.language_code] = elm.text_value;
        })
      }
    }
    activity.tips = tips.filter(elm => elm.is_tip)
    activity.recoms = tips.filter(elm => !elm.is_tip)

    dbResponse = await dbQuery.query(queryText3,[activity_id]);
    qas = dbResponse.rows;
    for(let i=0;i<qas.length;i++){
      dbResponse = await common.getLocalizedData('answer_localized','answer',qas[i].qa_id)
      if (dbResponse.rows[0] !== undefined) {
        dbResponse.rows.forEach(elm => {
          languages.push(elm.language_code)
          qas[i]['answer'+'-'+elm.language_code] = elm.text_value;
        })
      }
    }
    activity.questions = qas;

    let default_languages = [];
    languages.filter((v,i,a)=>a.indexOf(v)===i).forEach(elm => default_languages.push({title: elm.toUpperCase(),  key: elm}))
    activity.default_languages = default_languages;

    activity.floors = []
    let floor,spaces,space;
    const {rows} = await dbQuery.query(queryText6,[activity_id]);
    for(let i=0;rows.length!==0 && i<=rows.reduce((a,b)=>a.floor>b.floor?a:b).floor;i++){
      floor = {spaces:[]}
      spaces = rows.filter(elm => elm.floor===i)
      for(let j=0;j<spaces.length;j++){
        space = {space:spaces[j].space,thumb_ref:spaces[j].thumb_ref,image_ref:spaces[j].image_ref}
        dbResponse = await common.getLocalizedData('activity_localized','space_description',spaces[j].distribution_id)
        if (dbResponse.rows[0] !== undefined) {
          dbResponse.rows.forEach(elm => {
            space['space_description'+'-'+elm.language_code] = elm.text_value;
          })
        }
        dbResponse = await common.getLocalizedData('activity_localized','floor_description',spaces[j].distribution_id)
        if (dbResponse.rows[0] !== undefined) {
          dbResponse.rows.forEach(elm => {
            floor['floor_description'+'-'+elm.language_code] = elm.text_value;
          })
        }

        let {rows:items} = await dbQuery.query(queryText7,[spaces[j].distribution_id])
        for(let k=0;k<items.length;k++){
          dbResponse = await common.getLocalizedData('item_localized','description',items[k].item_id)
          if (dbResponse.rows[0] !== undefined) {
            dbResponse.rows.forEach(elm => {
              items[k]['description'+'-'+elm.language_code] = elm.text_value;
            })
          }
        }
        space.items = (items.length!==0)?items:[]
        floor.spaces.push(space)
      }
      activity.floors.push(floor)
    }

    sMessage.data = activity;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const editService = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = JSON.parse(req.body.data)
  const queryText1 = "update activity x set title=$1,latitude=$2,longitude=$3,website=$4,cost=$5,activity_status=$6,country=$7,city=$8,address=$9,max_persons=$10,visit_start_time=$11,visit_end_time=$12,currency=$13" +
    " from (select activity_id , latitude, longitude from activity where activity_id = $14) y where x.activity_id=y.activity_id returning y.latitude,y.longitude";
  const queryText2 = "insert into activity_images (activity_id,image_ref,thumb_ref) values ($1,$2,$3)"
  const queryText3 = "delete from activity_images where activity_id=$1 and image_ref=ANY($2)"
  //const queryText3 = "delete from activity_localized where entity_id=$1 and (text_key='activity_description' or text_key='activity_short_description')"
  const queryText4 = "delete from tip where associated_entity='activity' and associated_id=$1"
  const queryText5 = "insert into questions_answers (activity_id,intent,image_ref,thumb_ref) values ($1,$2,$3,$4) returning qa_id"

  const queryText6 = "insert into accommodation_distribution (activity_id,floor,space,image_ref,thumb_ref) values ($1,$2,$3,$4,$5) returning distribution_id"
  const queryText7 = "insert into items (distribution_id,image_ref,thumb_ref,quantity,value,entity) values ($1,$2,$3,$4,$5,$6) returning item_id"
  let rows, queryValues, image_ref, thumb_ref, QA;
  try {
    await dbQuery.query("BEGIN");

    const activity_id = body.activity_id
    const country = (body.hasOwnProperty('country'))?body.country:"Undef";
    const city = (body.hasOwnProperty('city'))?body.city:"Undef";
    const visit_start_time = (body.hasOwnProperty('visiting_hours'))?moment(body.visiting_hours[0]).format('HH:mm:ss'):undefined;
    const visit_end_time = (body.hasOwnProperty('visiting_hours'))?moment(body.visiting_hours[1]).format('HH:mm:ss'):undefined;
    const {latitude,longitude} = body
    queryValues = [body.title,latitude,longitude,body.website,body.cost,body.activity_status,country,city,body.address,body.max_persons,visit_start_time,visit_end_time,body.currency,activity_id];
    ({rows} = await dbQuery.query(queryText1,queryValues));

    await dbQuery.query(queryText3,[activity_id,body.removed_images]);
    if (body.total_images > 0){
      //await common.deleteAllBySingleKey('activity_images','activity_id',activity_id)
      for(let i=0;i<body.total_images;i++){
        [image_ref,thumb_ref] = common.getSingleImageRef('service',req.files,'activity'+i)
        queryValues = [activity_id,image_ref,thumb_ref]
        await dbQuery.query(queryText2,queryValues);
      }
    }

    if (latitude && longitude && (rows[0].latitude!==latitude || rows[0].longitude!==longitude)) await common.saveNearestPlaces(latitude,longitude);
    await common.saveLocalizedData(body.languages,body,'activity_localized','activity_description',activity_id);
    await common.saveLocalizedData(body.languages,body,'activity_localized','activity_short_description',activity_id);

    if(body.dates!==null){
      await common.deleteAllBySingleKey('activity_schedule','activity_id',activity_id)
      await common.deleteAllBySingleKey('schedule_brief','activity_id',activity_id)
      await common.saveActivitySchedule(body,activity_id)
    }

    await dbQuery.query(queryText4,[activity_id]);
    await common.saveTipsAndRecommendations(body,"activity",activity_id);

    await common.deleteAllBySingleKey('questions_answers','activity_id',activity_id)
    if (body.questions!=undefined){
      for(let i=0;i<body.questions.length;i++){
        QA = body.questions[i];
        [image_ref,thumb_ref] = common.getSingleImageRef('question',req.files,'question'+i);
        if (!common.checkImageRef(req.files,'question'+i) && QA.hasOwnProperty('image_ref')) {
          image_ref = QA.image_ref;
          thumb_ref = QA.thumb_ref;
        }
        queryValues = [activity_id,QA.intent,image_ref,thumb_ref];
        ({rows} = await dbQuery.query(queryText5,queryValues));
        await common.saveLocalizedData(body.languages,QA,'answer_localized','answer',rows[0].qa_id);
      }
    }

    //await common.deleteAllBySingleKey('accommodation_distribution','activity_id',activity_id)
    await common.deleteMultipleByKeyUnwrapped([activity_id],'accommodation_distribution','activity_id',[],'activity_localized','distribution_id','entity_id',"text_key='space_description' or text_key='floor_description'")
    let spaces,items,space,item,distribution_id;
    if (body.floors!==undefined){
      for(let i=0;i<body.floors.length;i++){
        spaces = body.floors[i].spaces;
        distribution_id = undefined
        for(let j=0;j<spaces.length;j++){
          space = spaces[j];
          [image_ref,thumb_ref] = common.getSingleImageRef('distribution',req.files,'space_'+i+'_'+j);
          if (!common.checkImageRef(req.files,'space_'+i+'_'+j) && space.hasOwnProperty('image_ref')) {
            image_ref = space.image_ref;
            thumb_ref = space.thumb_ref;
          }
          queryValues = [activity_id,i,space.space,image_ref,thumb_ref];
          ({rows} = await dbQuery.query(queryText6,queryValues));
          distribution_id = rows[0].distribution_id;
          await common.saveLocalizedData(body.languages,space,'activity_localized','space_description',distribution_id);

          items = space.items?space.items:[]
          for(let k=0;k<items.length;k++){
            item = items[k];
            [image_ref,thumb_ref] = common.getSingleImageRef('distribution',req.files,'item_'+i+'_'+j+'_'+k);
            if (!common.checkImageRef(req.files,'item_'+i+'_'+j+'_'+k) && item.hasOwnProperty('image_ref')) {
              image_ref = item.image_ref;
              thumb_ref = item.thumb_ref;
            }
            queryValues = [distribution_id,image_ref,thumb_ref,item.quantity,item.value,item.entity];
            ({rows} = await dbQuery.query(queryText7,queryValues));
            await common.saveLocalizedData(body.languages,item,'item_localized','description',rows[0].item_id);
          }
        }
        if (distribution_id!==undefined) await common.saveLocalizedData(body.languages,body.floors[i],'activity_localized','floor_description',distribution_id);
      }
    }

    await dbQuery.query("COMMIT");
    return res.status(status.created).send(sMessage);
  } catch (error) {
    await dbQuery.query("ROLLBACK");
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getRestaurantsInDate = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;

  const queryText1 = "select as2.* from activity_schedule as2 inner join activity a on as2.activity_id=a.activity_id where category='restaurant' and as2.date = $1 and (as2.start_time <= $2) and (as2.end_time >= $3)";
  let queryText2 = "select a.*, dl.text_value as desc, sdl.text_value as short_desc from activity a";
  queryText2 = common.addLocalizedQuery(queryText2,2,'a.activity_id','activity_localized','dl','activity_description')
  queryText2 = common.addLocalizedQuery(queryText2,2,'a.activity_id','activity_localized','sdl','activity_short_description')
  queryText2 += " where a.activity_id = ANY($1)"
  const queryText3 = "select activity_id,image_ref from activity_images where activity_id = ANY($1) ";
  let dbResponse,restaurants;
  try {
    dbResponse = await dbQuery.query(queryText1,[body.date,body.start_time,body.end_time]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data.schedule = dbResponse.rows;

    let activities = dbResponse.rows.map(elm => elm.activity_id);
    activities = [...new Set(activities)]

    dbResponse = await dbQuery.query(queryText2,[activities,body.language_code]);
    restaurants = (dbResponse.rows[0] === undefined)?[]:dbResponse.rows;

    dbResponse = await dbQuery.query(queryText3,[activities]);
    const images = (dbResponse.rows[0] === undefined)?[]:dbResponse.rows;
    for(let i=0;i<restaurants.length;i++){
      restaurants[i].images = images.filter(image => image.activity_id===restaurants[i].activity_id).map(elm => elm.image_ref)
    }

    sMessage.data.restaurants = restaurants;

    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
  //return await common.simpleTableSelect(req,res,"activity","category","restaurant")
}

const updateServicesStatus = async (req, res) =>{
  return await common.updateEntityStatus(req,res,'activity','activity_id');
}

const deleteServicesByID = async (req, res) =>{
  return await common.deleteMultipleByKey(req,res,'activity','activity_id');
}

const getServiceInfo = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText1 = "select a.*, dl.text_value as desc, sdl.text_value as short_desc from activity a "
  queryText1 = common.addLocalizedQuery(queryText1,1,'a.activity_id','activity_localized','dl','activity_description')
  queryText1 = common.addLocalizedQuery(queryText1,1,'a.activity_id','activity_localized','sdl','activity_short_description')
  queryText1 += " where a.activity_id=$2";
  const queryText2 = "select image_ref from activity_images where activity_id = $1 ";
  let dbResponse,rows;

  try {
    const activity_id =  body.activity_id;
    const language_code = body.language_code;

    dbResponse = await dbQuery.query(queryText1,[language_code,activity_id]);
    rows = dbResponse.rows;
    if (rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    const service = rows[0];

    dbResponse = await dbQuery.query(queryText2,[activity_id]);
    rows = dbResponse.rows;
    const images = (rows[0] === undefined)?[]:rows;
    service.images = images.map(elm => elm.image_ref)

    sMessage.data = service;

    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getQuestionsFromAccommodation = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "select wq.intent,wq."+body.language_code+" as question from watson_questions wq" +
    " where not wq.faq order by wq.calls desc limit 5"
  const queryText2 = "select qa.intent,wq."+body.language_code+" as question,qa.thumb_ref from questions_answers qa inner join watson_questions wq on qa.intent=wq.intent" +
    " where qa.activity_id=$1 and wq.faq order by wq.calls desc limit $2"
  try {
    const {rows:rows1} = await dbQuery.query(queryText1,[])
    const {rows:rows2} = await dbQuery.query(queryText2,[body.activity_id,5-rows1.length])
    sMessage.data = [...rows1,...rows2];
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getDistributionFromAccommodation = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const language_code = body.language_code
  let queryText1 = "select ad.*,ll.*,sl.text_value as space_description from accommodation_distribution ad inner join labels_localized ll on ad.space=ll.text_key "
  queryText1 = common.addLocalizedQuery(queryText1,1,'ad.distribution_id','activity_localized','sl','space_description')
  queryText1 += " where ad.activity_id=$2 and ll.platform='web' order by pg_catalog.array_position(array['kitchen','main_bedroom','bedroom','main_bathroom','bathroom','guest_bathroom','living_room','dining_room'], ad.space::text)"
  let queryText2 = "select coalesce(il.text_value,ll."+language_code+") as description, i.thumb_ref, i.quantity, i.value, a.currency from items i inner join labels_localized ll on i.entity=ll.text_key"
  queryText2 = common.addLocalizedQuery(queryText2,1,'i.item_id','item_localized','il','description')
  queryText2 += " cross join activity a where ll.platform='web' and i.distribution_id=$2 and a.activity_id=$3"
  let queryText3 = "select fl.text_value as floor_description from accommodation_distribution ad "
  queryText3 = common.addLocalizedQuery(queryText3,1,'ad.distribution_id','activity_localized','fl','floor_description')
  queryText3 += " where ad.distribution_id=ANY($2) and fl.text_value is not null"
  const distribution = {floors:[]}
  try {
    const {rows} = await dbQuery.query(queryText1,[language_code,body.activity_id])
    if (rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    let floor,spaces,space,dbResponse;
    for(let i=0;i<=rows.reduce((a,b)=>a.floor>b.floor?a:b).floor;i++){
      floor = {floor:i+1, spaces:[]}
      spaces = rows.filter(elm => elm.floor===i)
      dbResponse = await dbQuery.query(queryText3,[language_code,[spaces.map(elm=>elm.distribution_id)]])
      floor.description = dbResponse.rows[0]?dbResponse.rows[0].floor_description:null
      for(let j=0;j<spaces.length;j++){
        space = {space:spaces[j][language_code],space_key:spaces[j].space,thumb_ref:spaces[j].thumb_ref,description:spaces[j].space_description}
        dbResponse = await dbQuery.query(queryText2,[language_code,spaces[j].distribution_id,body.activity_id])
        space.items = (dbResponse.rows[0]!==undefined)?dbResponse.rows:[]
        floor.spaces.push(space)
      }
      distribution.floors.push(floor)
    }
    sMessage.data = distribution;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

module.exports = {
  addAccommodationService,
  addRestaurantService,
  addOtherService,
  editService,
  deleteServicesByID,
  updateServicesStatus,
  getServicesByFilter,
  getRestaurantsInDate,
  getAccommodationsInDateRange,
  getServiceEditableDetails,
  getServiceInfo,
  getQuestionsFromAccommodation,
  getDistributionFromAccommodation
}