const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const common = require('./common')
const { uploadLog } = require('../COS')
const {hash} = require('../Helpers/encrypt')
const cloneDeep = require('clone-deep');

const register = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const queryText1 = "insert into users (email,password,name,last_name,mobile) values ($1,$2,$3,$4,$5) returning user_id";
  const queryText2 = "insert into client (client_id,num_trips,client_status) values ($1,$2,$3);";

  try {
    if (await common.checkFieldExists('email','users','user_id',req.body.email)){
      const hashed_password = await hash(req.body.password);
      const queryValues = [req.body.email,hashed_password,req.body.name,req.body.last_name,req.body.mobile];
      let {rows} = await dbQuery.query(queryText1,queryValues);
      const id = rows[0].user_id;
      sMessage.data.user_id = id;
      await dbQuery.query(queryText2,[id,0,true]);
      return res.status(status.created).send(sMessage);
    }else{
      eMessage.errors.push({type:'exists',msg:'User already exists'});
      return res.status(status.conflict).send(eMessage);
    }
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const login = async (req, res) =>{
  return await common.login(req,res,true)
}

const checkEmailExists = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const queryText = "select count(user_id) from users where email = $1";
  try {
    let {rows} = await dbQuery.query(queryText,[req.body.email]);
    const dbResponse = rows;
    sMessage.data = dbResponse[0].count === '0';
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getAllClients = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const queryText = "select * from users u inner join client c on u.user_id = c.client_id";
  try {
    const {rows} = await dbQuery.query(queryText);
    const dbResponse = rows;
    if (dbResponse[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getClientByID = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const queryText = "select * from users u inner join client c on u.user_id = c.client_id where c.client_id = $1";
  try {
    let {rows} = await dbQuery.query(queryText,[req.body.client_id]);
    const dbResponse = rows;
    if (dbResponse[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getClientsByFilter = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body =  req.body.data
  const queryText1 = "select p.prov_id from provider p left join tour_operator_provider top on top.prov_id=p.prov_id where top.tour_operator_id=$1 or p.prov_id=$1";
  let queryText2 = "select c.client_id, u.avatar_ref,u.name,u.last_name,u.email,u.mobile,c.city_country,c.num_trips,c.client_status from users u inner join client c on u.user_id = c.client_id";
  let queryValues = [], dbResponse;
  const filter = body.filter;
  try {
    if (body.look_for==='mine'){
      dbResponse = await dbQuery.query(queryText1,[body.myID])
      sMessage.data = dbResponse.rows;
      queryValues.push(dbResponse.rows.map(elm => elm.prov_id))
      queryText2 += " inner join user_activity ua on ua.client_id=c.client_id inner join activity_instance ai on ua.activ_instance_id=ai.activ_instance_id" +
        " inner join activity a on a.activity_id=ai.activity_id where a.prov_id = ANY($1)";
    }
    if (filter!=='all'){
      queryText2 += (queryValues.length===1)?' and':' where'
      queryValues.push('%'+body.value+'%')
      queryText2 += " (u.name like $"+queryValues.length+" or u.last_name like $"+queryValues.length+")"
    }

    dbResponse = await dbQuery.query(queryText2,queryValues)
    const clients = (dbResponse.rows[0]===undefined)?[]:dbResponse.rows;

    sMessage.data = clients;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const deleteUsersByEmail = async (req, res) =>{
  await common.deleteMultipleByKey(req,res,'users','email');
}

const updateClient = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "update users set name = $1 , last_name = $2 , mobile = $3  where email = $4 returning user_id";
  const queryText2 = "update client set client_status = $1  where client_id = $2";
  const queryValues = [body.name,body.last_name,body.mobile,body.email];
  try {
    let {rows} = await dbQuery.query(queryText1,queryValues);
    const id = rows[0].user_id;
    await dbQuery.query(queryText2,[body.client_status,id]);
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const addClientPreferences = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "insert into user_preferences (preference_code,preference_value,client_id) values ($1,$2,$3)";
  let preference ;
  try {
    for(let i=0;i<body.preferences.length;i++){
      preference = body.preferences[i];
      await dbQuery.query(queryText,[preference.preference_code,preference.preference_value,body.client_id]);
    }
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}


const askWatsonAssistant = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  try {
    const question = body.question,client_id=body.client_id,activity_id=body.activity_id,language_code=body.language_code;
    const response = await common.askAssistant(question,client_id,activity_id,language_code)
    const log = {
      client_id: client_id,
      activity_id: activity_id,
      question: question,
      language_code:language_code
    }
    uploadLog(log)
    sMessage.data = response;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const askWatsonAssistantV2 = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  try {
    const question = body.question,client_id=body.client_id,parameters=body.parameters,language_code=body.language_code;
    const app_actions = await common.askAssistantV2(question,client_id,language_code,parameters)
    const log = {
      client_id: client_id,
      parameters: parameters,
      question: question,
      language_code:language_code
    }
    //uploadLog(log)
    sMessage.data.actions = app_actions;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

module.exports = {
  getAllClients,
  getClientByID,
  register,
  login,
  checkEmailExists,
  getClientsByFilter,
  deleteUsersByEmail,
  updateClient,
  addClientPreferences,
  askWatsonAssistant,
  askWatsonAssistantV2,
}