const fetch = require('node-fetch');
const axios = require('axios');
const sharp = require('sharp');
const cloneDeep = require('clone-deep');
const {errorMessage,successMessage,status} = require('../Helpers/status');
const client = require('./client')
const poi = require('./poi')
const clientTrip = require('./clientTrip')
const tour = require('./tour')
const provider = require('./provider')
const admin = require('./admin')
const label = require('./label')
const permission = require('./permission')
const service = require('./service')
const watson = require('./watson')
const translate = require('@iamtraction/google-translate');

const { listImages } = require('../COS')
//---------------TESTING--------------

const ready_live = async (req, res) =>{
	const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
	try {
		return res.status(status.success).send(sMessage);
	} catch (error) {
		return res.status(status.error).send(eMessage);
	}
}


const resizeImage = async (req, res) =>{
	try {
		const imageResponse = await axios({url: req.query.url, responseType: 'stream'})
		const format = req.query.format
		const props = {}
		if (req.query.width!==undefined){
			props.width = parseInt(req.query.width)
		}
		if (req.query.height!==undefined){
			props.height = parseInt(req.query.height)
		}
		props.fit = (props.width===undefined || props.height===undefined)?sharp.fit.contain:sharp.fit.fill

		const resizer =
			sharp({ failOnError: false })
				.resize(props)
				.toFormat(format);
		res.type(format);
		imageResponse.data.pipe(resizer).pipe(res)

		return res.status(status.success)
	} catch (error) {
		console.log(error)
		return res.status(status.error).send(eMessage);
	}
}

const translateMultiLanguageTab = async (req, res) =>{
	const eMessage = cloneDeep(errorMessage)
	const sMessage = cloneDeep(successMessage)
	const body = req.body.data;
	let language_code;
	try {
		const main_language = body.main_language
		const text_main = body.inputs.find(elm => elm.key===main_language).text
		if (text_main===undefined || text_main===''){
			return res.status(status.error).send(eMessage);
		}
		const data = body.inputs.filter(elm => elm.key!==main_language)
		for (let i = 0; i < data.length; i++) {
			if (data[i].text===undefined || data[i].text===''){
				language_code = data[i].key
				let {text} = await translate(text_main, { from: main_language, to: (language_code==='jp')?'ja':(language_code==='zh')?'zh-cn':language_code })
				data[i].text = text
			}
		}
		sMessage.data = data
		return res.status(status.success).send(sMessage)
	} catch (error) {
		console.log(error)
		return res.status(status.error).send(eMessage);
	}
}

const common = require('./common')
const test = async (req, res) =>{
	const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
	const body = req.body.data;
	try {
		const answer = await common.askAssistantTest(body.question,body.client_id,body.language_code)
		sMessage.data = answer;
		return res.status(status.success).send(sMessage);
	} catch (error) {
		console.log(error)
		return res.status(status.error).send(eMessage);
	}
}

const getNearestPlaces = async (req, res) =>{
	const eMessage = cloneDeep(errorMessage)
	const sMessage = cloneDeep(successMessage)
	try {
		const {latitude,longitude,language_code} = req.query;
		const key = process.env.GOOGLE_API_KEY
		const type = 'point_of_interest'
		const {data} = await axios.get(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${latitude},${longitude}&rankby=distance&type=${type}&key=${key}&language=${language_code}`)
		sMessage.data = data.results.map(elm => ({location:elm.geometry.location,name:elm.name,icon:elm.icon,type:elm.types[0]}))
		return res.status(status.success).send(sMessage);
	}
	catch (error) {
		console.log(error)
		return res.status(status.error).send(eMessage);
	}
}

module.exports = {
	client,
	poi,
	clientTrip,
	tour,
	provider,
	admin,
	label,
	permission,
	service,
	watson,

	ready_live,
	resizeImage,
	translateMultiLanguageTab,
	test,
	getNearestPlaces
};
