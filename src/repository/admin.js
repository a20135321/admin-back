const jwt = require('jsonwebtoken');
const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const common = require('./common')
const {hash,generateJWT} = require('../Helpers/encrypt')
const {sendEmailNotification} = require('../Sendgrid/mailer')
const cloneDeep = require('clone-deep');

const EULER_ADMIN = parseInt(process.env.EULER_ADMIN)
const ADMIN_DEFAULT = parseInt(process.env.ADMIN_DEFAULT)

const register = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = JSON.parse(req.body.data);
  const queryText1 = "insert into users (email,password,name,last_name,mobile,avatar_ref) values ($1,$2,$3,$4,$5,$6) returning user_id";
  const queryText2 = "insert into admin (admin_id,provider_id,is_company,admin_status) values ($1,$2,$3,$4)";
  const queryText3 = "insert into client (client_id,client_status,num_trips) values ($1,$2,$3)";
  let queryValues,dbResponse;
  try {
    if (await common.checkFieldExists('email','users','user_id',body.email)){
      const [avatar_ref,] = common.getSingleImageRef('user',req.files,'avatar')

      const hashed_password = await hash(body.password);
      queryValues = [body.email,hashed_password,body.first_name,body.last_name,body.mobile,avatar_ref];
      dbResponse = await dbQuery.query(queryText1,queryValues);
      const user_id = dbResponse.rows[0].user_id;

      queryValues = [user_id,body.provider.value,false,'pending'];
      await dbQuery.query(queryText2,queryValues);
      queryValues = [user_id,true,0];
      await dbQuery.query(queryText3,queryValues);

      let permission_groups = body.permission_groups;
      if (!permission_groups.includes(ADMIN_DEFAULT)){
        permission_groups.push(ADMIN_DEFAULT)
      }
      await common.assignAccessGroupsToUser(user_id,permission_groups)

      return res.status(status.created).send(sMessage);
    }
    eMessage.errors.push({type:'exists',msg:'User already exists'});
    return res.status(status.conflict).send(eMessage);

  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const login = async (req, res) =>{
  return await common.login(req,res,false)
}

const editAdmin = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = JSON.parse(req.body.data);
  let queryText1 = "update users set name=$1,last_name=$2,mobile=$3";
  const queryText2 = "update admin set admin_status=$1 where admin_id=$2";
  let queryValues;
  try {
    queryValues = [body.first_name,body.last_name,body.mobile]
    if(body.hasOwnProperty('password')){
      const hashed_password = await hash(body.password);
      queryValues.push(hashed_password)
      queryText1 += ",password=$"+queryValues.length
    }
    if(common.checkImageRef(req.files,'avatar')){
      const [avatar_ref,] = common.getSingleImageRef('user',req.files,'avatar')
      queryValues.push(avatar_ref)
      queryText1 += ",avatar_ref=$"+queryValues.length
    }
    queryValues.push(body.admin_id)
    queryText1 += " where user_id=$"+queryValues.length
    await dbQuery.query(queryText1,queryValues);

    await common.notifyAdminStatusChanged(body.admin_id,body.admin_status)
    queryValues = [body.admin_status,body.admin_id];
    await dbQuery.query(queryText2,queryValues);

    await common.deleteAllBySingleKey('user_access','user_id',body.admin_id)
    await common.assignAccessGroupsToUser(body.admin_id,body.permission_groups)

    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const deleteAdminsByID = async (req, res) =>{
    return await common.deleteMultipleByKey(req,res,'users','user_id',[EULER_ADMIN])
}

const getMyAdmins = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText1 = "select u.name||' '||u.last_name as full_name, p.company_name, p.city||'/'||p.country as city_country, u.email, u.avatar_ref, u.last_access, a.admin_status, a.admin_id" +
    " from admin a inner join users u on a.admin_id = u.user_id inner join provider p on a.provider_id = p.prov_id";
  let queryValues = [], dbResponse;
  const filter = body.hasOwnProperty('filter')?body.filter:'all';
  try {
    if (body.hasOwnProperty('myID') && body.hasOwnProperty('type')){
      if (body.type==='operator'){
        queryText1 += " left join tour_operator_provider top on top.prov_id=p.prov_id where (top.tour_operator_id=$1 or a.provider_id=$1)";
      }else{
        queryText1 += " where a.provider_id=$1";
      }
      queryValues.push(body.myID);
    }
    if (filter !== 'all'){
      (filter!=='admin_status')?queryValues.push('%'+body.value+'%'):queryValues.push(body.value)
      if (filter==='name'){
        queryText1 += " and (u.name like $"+queryValues.length+" or u.last_name like $"+queryValues.length+")"
      }else if (filter==='provider'){
        queryText1 += " and p.company_name like $"+queryValues.length;
      }else if(filter==='admin_status'){
        queryText1 += " and a.admin_status like $"+queryValues.length;
      }
    }
    queryText1 += " order by full_name asc"
    dbResponse = await dbQuery.query(queryText1,queryValues);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }

    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  }catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getAdminEditableDetails = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText1 = "select u.name as first_name, u.last_name, u.mobile, p.company_name, u.email, u.avatar_ref, a.admin_status, a.admin_id" +
    " from admin a inner join users u on a.admin_id = u.user_id inner join provider p on a.provider_id = p.prov_id where a.admin_id=$1";
  const queryText2 = "select * from user_access where user_id = $1"
  let dbResponse, admin, permits;
  try {
    const admin_id = body.admin_id;
    dbResponse = await dbQuery.query(queryText1,[admin_id]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    admin = dbResponse.rows[0];

    dbResponse = await dbQuery.query(queryText2,[admin_id]);
    permits = (dbResponse.rows[0]===undefined)?[]:dbResponse.rows;
    admin.permission_groups = permits.map(elm => elm.group_id)

    sMessage.data = admin;
    return res.status(status.success).send(sMessage);
  }catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const updateAdminStatus = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  if(req.body.data.id!==EULER_ADMIN){
    return await common.updateEntityStatus(req,res,'admin','admin_id')
  }else{
    eMessage.errors.push({type:'uneditable',msg:'Can not change this!'});
    return res.status(status.conflict).send(eMessage);
  }
}

const sendRecoverEmail = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "select email,name,user_id from users where email = $1";
  let dbResponse;
  try {
    dbResponse = await dbQuery.query(queryText,[body.email])
    const user = dbResponse.rows[0];
    if (user === undefined){
      eMessage.errors.push({type:'email',msg:'User e-mail does not exist'});
      return res.status(status.notfound).send(eMessage);
    }
    const reset_token = generateJWT(user)
    const link = "http://" + req.headers.host + "/api/v1/user/recover/" + reset_token;
    console.log(link)
    const message = `Hi ${user.name} \n 
                    Please click on the following link ${link} to reset your password. \n\n 
                    If you did not request this, please ignore this email and your password will remain unchanged.\n`
    sendEmailNotification(message,body.email)
    return res.status(status.success).send(sMessage);
  }catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const resetPassword = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "update users set password=$1 where user_id=$2"
  try {
    const decoded = jwt.verify(body.reset_token, process.env.TOKEN_SECRET);
    const hashed_password = await hash(body.password);
    await dbQuery.query(queryText,[hashed_password,decoded.user_id])
    return res.status(status.success).send(sMessage);
  }catch (error) {
    console.log(error)
    eMessage.errors.push({type:'token',msg:'Token invalid or expired'});
    return res.status(status.unauthorized).send(eMessage);
  }
}

module.exports = {
  register,
  login,
  editAdmin,
  deleteAdminsByID,
  getMyAdmins,
  updateAdminStatus,
  getAdminEditableDetails,
  sendRecoverEmail,
  resetPassword
}