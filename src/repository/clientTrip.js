const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const common = require('./common')
const cloneDeep = require('clone-deep');

const DEFAULT_IMAGE_REF=process.env.DEFAULT_IMAGE_REF

const newClientTrip = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "insert into user_trip (destiny,start_date,end_date,client_id) values ($1,$2,$3,$4) returning user_trip_id";
  try {
    const dbResponse = await dbQuery.query(queryText,[body.destiny,body.start_date,body.end_date,body.client_id]);
    const id = dbResponse.rows[0].user_trip_id;
    sMessage.data.user_trip_id = id;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const updateClientTrip = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  try {
    const field = body.field
    const queryText = "update user_trip set "+field+" = $1 where user_trip_id = $2";
    await dbQuery.query(queryText,[body.value,body.user_trip_id]);
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const setClientTripSchedule = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText1 = "select * from activity_instance where activity_id=$1 and start_time=$2 and start_date=$3 and end_date=$4"
  const queryText2 = "insert into activity_instance (activity_id,start_time,start_date,end_date,total_persons) values ($1,$2,$3,$4,$5) returning activ_instance_id";
  const queryText3 = "update activity_instance set total_persons = $1 where activ_instance_id = $2"
  const queryText4 = "insert into user_activity (number_of_persons,activ_instance_id,user_trip_id,client_id) values ($1,$2,$3,$4)";
  const queryText5 = "insert into external_activity (description,start_time,end_time,date,user_trip_id,associated_activity_id) values ($1,$2,$3,$4,$5,$6)";
  let dbResponse,activ_instance_id,activity;
  try {
    const client_id = body.client_id
    const user_trip_id = body.user_trip_id

    await common.deleteAllBySingleKey('user_activity','user_trip_id',user_trip_id)
    await common.deleteAllBySingleKey('external_activity','user_trip_id',user_trip_id)
    for(let i=0;i<body.activities.length;i++){
      activity = body.activities[i];
      dbResponse = await dbQuery.query(queryText1,[activity.activity_id,activity.start_time,activity.start_date,activity.end_date])
      dbResponse = dbResponse.rows[0]
      if (dbResponse===undefined){
        dbResponse = await dbQuery.query(queryText2,[activity.activity_id,activity.start_time,activity.start_date,activity.end_date,0]);
        dbResponse = dbResponse.rows[0]
      }else{
        await dbQuery.query(queryText3,[dbResponse.total_persons+activity.number_of_persons,dbResponse.activ_instance_id])
      }
      activ_instance_id = dbResponse.activ_instance_id
      await dbQuery.query(queryText4,[activity.number_of_persons,activ_instance_id,user_trip_id,client_id]);
    }

    for(let i=0;i<body.other_activities.length;i++){
      activity = body.other_activities[i];
      await dbQuery.query(queryText5,[activity.description,activity.start_time,activity.end_time,activity.date,user_trip_id,activity.associated_activity_id]);
    }

    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getClientTripSchedule = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let dbResponse,activities,images,activity_images;
  try {
    const cond1 = (body.hasOwnProperty('date'))?" and as2.date = $2":"";
    const cond2 = (body.hasOwnProperty('date'))?" and date = $2":"";
    const queryValues = (body.hasOwnProperty('date'))?[body.user_trip_id,body.date]:[body.user_trip_id]
    const queryText1 = "select ai.start_time,ai.start_date,ai.end_date,a.title,a.duration,a.category,a.activity_id from user_activity ua inner join activity_instance ai" +
      " on ua.activ_instance_id=ai.activ_instance_id inner join activity a on ai.activity_id=a.activity_id where ua.user_trip_id = $1"+cond1
    const queryText2 = "select * from activity_images where activity_id = ANY($1)"
    const queryText3 = "select ea.description,ea.start_time,ea.end_time,ea.date,a.activity_id,a.title from external_activity ea left join activity a" +
      " on ea.associated_activity_id=a.activity_id where user_trip_id = $1"+cond2

    dbResponse = await dbQuery.query(queryText1,queryValues)
    activities = dbResponse.rows;

    dbResponse = await dbQuery.query(queryText2,[activities.map(elm=>elm.activity_id)])
    images = (dbResponse.rows[0]!==undefined)?dbResponse.rows:[];
    for(let i=0;i<activities.length;i++){
      activity_images = images.filter(image => image.activity_id===activities[i].activity_id)
      activities[i].image = (activity_images.length!==0)?activity_images[0].image_ref:DEFAULT_IMAGE_REF;
    }
    sMessage.data.activities = activities

    dbResponse = await dbQuery.query(queryText3,queryValues)
    sMessage.data.other_activities = dbResponse.rows;

    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getMyUserTrips = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  const queryText = "select * from user_trip where client_id=$1"

  try {
    const dbResponse = await dbQuery.query(queryText,[body.user_id])
    const rows = dbResponse.rows;
    if (rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(body)
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getClientsTripsByFilter = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body =  req.body.data
  const queryText1 = "select p.prov_id from provider p left join tour_operator_provider top on top.prov_id=p.prov_id where top.tour_operator_id=$1 or p.prov_id=$1";
  let queryText2 = "select ut.* ,c.city_country, u.name || ' ' || u.last_name as name from users u inner join client c on u.user_id = c.client_id inner join user_trip ut on ut.client_id=c.client_id";
  let queryValues = [], dbResponse;
  const filter = body.filter;
  try {
    if (body.look_for==='mine'){
      dbResponse = await dbQuery.query(queryText1,[body.myID])
      sMessage.data = dbResponse.rows;
      queryValues.push(dbResponse.rows.map(elm => elm.prov_id))
      queryText2 += " inner join user_activity ua on ua.client_id=c.client_id inner join activity_instance ai on ua.activ_instance_id=ai.activ_instance_id" +
        " inner join activity a on a.activity_id=ai.activity_id where a.prov_id = ANY($1)";
    }
    if (filter!=='all'){
      queryText2 += (queryValues.length===1)?' and':' where'
      queryValues.push('%'+body.value+'%')
      queryText2 += " (u.name like $"+queryValues.length+" or u.last_name like $"+queryValues.length+")"
    }

    dbResponse = await dbQuery.query(queryText2,queryValues)
    const trips = (dbResponse.rows[0]===undefined)?[]:dbResponse.rows;

    sMessage.data = trips;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const deleteClientTripsByID = async (req, res) =>{
  return await common.deleteMultipleByKey(req,res,'user_trip','user_trip_id');
}

module.exports = {
  newClientTrip,
  updateClientTrip,
  setClientTripSchedule,
  getClientTripSchedule,
  getClientsTripsByFilter,
  deleteClientTripsByID,
  getMyUserTrips,
}