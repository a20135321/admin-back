const dbQuery  = require('../config/datos-relacionales')
const {errorMessage,successMessage,status} = require('../Helpers/status');
const cloneDeep = require('clone-deep');
const common = require('./common')
const {uploadAppLabels} = require('../COS/index')
const { parse } = require('json2csv');

const getAppLocalizedLabels = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let dbResponse,queryText,queryValues;
  queryValues = [body.request_date];
  queryText = "select distinct on (group_code,text_key) group_code,text_key,en,es,it from labels_localized where platform='app' and created_date > $1";
  if (body.hasOwnProperty('group_code')){
    queryText += " and group_code = $2"
    queryValues.push(body.group_code)
  }
  queryText += " order by text_key,group_code,created_date desc";
  try {
    dbResponse = await dbQuery.query(queryText,queryValues);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getWebLocalizedLabels = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  sMessage.data.en = {};
  sMessage.data.es = {};
  let dbResponse,queryText;
  queryText = "select distinct on (group_code,text_key) group_code||'.'||text_key as key ,en,es from labels_localized where platform='web'";
  try {
    dbResponse = await dbQuery.query(queryText,[]);
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    dbResponse.rows.forEach(elm => {
      sMessage.data.en[elm.key] = elm.en;
      sMessage.data.es[elm.key] = elm.es;
    })
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const updateLabelsVersion = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const queryText1 = "select distinct on (group_code,text_key) group_code,text_key as Keys,en as English,es as Spanish, it as Italian from labels_localized";
  const queryText2 = "insert into app_resources (key,url) values ($1,$2)"
  const queryText3 = "update app_resources set version=version+1 where key=$1"
  let group,content,file_url,key;
  const fields = ['keys','english','spanish','italian']
  const opts = { fields };
  try {
    const { rows } = await dbQuery.query(queryText1,[]);
    if (rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.noContent).send(eMessage);
    }
    const groups = Array.from(new Set(rows.map((item) => item.group_code)))
    for (let i = 0; i < groups.length; i++) {
      key = groups[i]
      group = rows.filter(item => item.group_code===key)
      content = parse(group, opts);
      file_url = uploadAppLabels(content,key)
      if (await common.checkFieldExists('key','app_resources','key',key)){
        await dbQuery.query(queryText2,[key,file_url]);
      }else{
        await dbQuery.query(queryText3,[key]);
      }
    }
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const checkForNewVersions = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const queryText = "select * from app_resources";
  try {
    const { rows } = await dbQuery.query(queryText,[]);
    if (rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.noContent).send(eMessage);
    }
    sMessage.data = rows
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getLabelItemsSelector = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText = "select text_key as key,"+body.language_code+" as text from labels_localized where platform='web' and group_code='entity' order by text asc"
  try {
    const dbResponse = await dbQuery.query(queryText,[])
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

const getLabelSpacesSelector = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  let queryText = "select text_key as key,"+body.language_code+" as text from labels_localized where platform='web' and group_code='space' order by text asc"
  try {
    const dbResponse = await dbQuery.query(queryText,[])
    if (dbResponse.rows[0] === undefined) {
      eMessage.errors.push({type:'notfound',msg:'Not found in DB'});
      return res.status(status.notfound).send(eMessage);
    }
    sMessage.data = dbResponse.rows;
    return res.status(status.success).send(sMessage);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

module.exports = {
  getAppLocalizedLabels,
  getWebLocalizedLabels,
  updateLabelsVersion,
  checkForNewVersions,
  getLabelItemsSelector,
  getLabelSpacesSelector,
}