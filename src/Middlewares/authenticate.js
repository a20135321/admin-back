const passport = require("passport");
const {errorMessage,status} = require('../Helpers/status');
const cloneDeep = require('clone-deep');

module.exports = (req, res, next) => {
  const eMessage = cloneDeep(errorMessage)
  passport.authenticate('jwt', {session: false},function(err, user, info) {
    if (err) return next(err);
    eMessage.errors.push({type:"token",msg:"Unauthorized Access - No Token Provided!"})
    if (!user) return res.status(status.unauthorized).send(eMessage);
    req.user = user;
    next();
  })(req, res, next);
};