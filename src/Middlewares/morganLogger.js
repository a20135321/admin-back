const chalk = require('chalk')
const morgan = require('morgan')

const ctx = new chalk.constructor({enabled: true});

module.exports = morgan(function (tokens, req, res) {
  if (tokens.url(req, res) !== '/api/v1/ready_live') {
    var status = tokens.status(req, res)
    var statusColor = status >= 500
      ? 'red' : status >= 400
        ? 'yellow' : status >= 300
          ? 'cyan' : 'green'
    return [
      ctx.yellow(tokens.method(req, res)),
      ctx.magenta(tokens.url(req, res)),
      ctx[statusColor](status),
      ctx.blue(tokens['response-time'](req, res) + ' ms'),
      ctx.white('- ' + tokens.date(req, res)),
    ].join(' ')
  }
});