const JwtStrategy = require('passport-jwt').Strategy,ExtractJwt = require('passport-jwt').ExtractJwt;
const dbQuery  = require('../config/datos-relacionales')

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.TOKEN_SECRET
};

module.exports = passport => {
  passport.use(
    new JwtStrategy(opts, async (jwt_payload, done) => {
      try {
        //console.log("PAYLOAD",jwt_payload)
        const queryText = "select * from users where user_id=$1"
        const dbResponse = await dbQuery.query(queryText,[jwt_payload.user_id])
        if (dbResponse.rows[0]!==undefined){
          return done(null, dbResponse.rows[0])
        }
        return done(null, false)
      }catch (error){
        return done(error, false, {message: 'Server Error'});
      }
    })
  );
};