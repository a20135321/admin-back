const {validationResult} = require('express-validator');
const {errorMessage,status} = require('../Helpers/status');
const cloneDeep = require('clone-deep');

module.exports = (req, res, next) => {
  const eMessage = cloneDeep(errorMessage)
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    errors.array().forEach(err => {eMessage.errors.push({type:err.param,msg:err.msg})})
    return res.status(status.error).send(eMessage);
  }
  next();
};