const { Pool } = require('pg')
const connectionString = process.env.CONNECTION_STRING;
const cert_base64 = process.env.CERT_BASE64;
const PG_SCHEMA = process.env.PG_SCHEMA;
const caCert = Buffer.from(cert_base64, 'base64').toString('utf-8')

console.log('[INFO] creating pool...')
const pool = new Pool({
	connectionString: connectionString,
	ssl: { ca: caCert },
	idleTimeoutMillis : 60000,
})

pool.on('error',(err,client)=>{
	console.log("Unexpected error on idle client",client);
	process.exit(-1)
})

pool.on('connect',client=>{
	client.query("set search_path to "+PG_SCHEMA, [])
})
console.log('[INFO] pool created')

const query = async (queryText, params, callback) =>{
	const client = await pool.connect()
	try {
		const res = await client.query(queryText,params)
		return res
	}catch (e){
		throw e
	}finally {
		client.release()
	}
}

module.exports = {
	query
}
