const Readable = require('stream').Readable;
const {IamAuthenticator } = require('ibm-watson/auth');
const SpeechToTextV1 = require('ibm-watson/speech-to-text/v1');
const common = require('../repository/common')
const { uploadLog } = require('../COS')
const {errorMessage,successMessage} = require('../Helpers/status');
const cloneDeep = require('clone-deep');

function tryParseJSON (jsonString){
  try {
    var o = JSON.parse(jsonString);
    if (o && typeof o === "object") {
      return o;
    }
  }
  catch (e) { }
  return undefined;
}
function onEvent(name, event) {
  if (name==='Error:') console.log(name, JSON.stringify(event.raw.data, null, 2))
    //console.log(name, JSON.stringify(event, null, 2));
}
async function generateResult(question,client_id,activity_id,language_code,input_chunks){
  const sMessage = cloneDeep(successMessage)
  const eMessage = cloneDeep(errorMessage)
  try {
    const response = await common.askAssistant(question,client_id,activity_id,language_code)
    const log = {
      client_id: client_id,
      activity_id: activity_id,
      question: question,
      language_code:language_code,
      audio: Buffer.concat(input_chunks)
    }
    uploadLog(log)
    sMessage.data=response
    //console.log("FINAL RESPONSE:",sMessage)
    return sMessage
  }catch (error){
    return eMessage
  }
}
async function generateResultV2(question,client_id,parameters,language_code,input_chunks){
  const sMessage = cloneDeep(successMessage)
  const eMessage = cloneDeep(errorMessage)
  try {
    const app_actions = await common.askAssistantV2(question,client_id,language_code,parameters)
    const log = {
      client_id: client_id,
      parameters:parameters,
      question: question,
      language_code:language_code,
      audio: Buffer.concat(input_chunks)
    }
    uploadLog(log)
    sMessage.data.actions=app_actions
    //console.log("FINAL RESPONSE:",sMessage)
    return sMessage
  }catch (error){
    return eMessage
  }
}

const STT_models = {
  en:"en-US_BroadbandModel",
  es:"es-ES_BroadbandModel",
  it:"it-IT_BroadbandModel"
}

module.exports = (wss) => {
  wss.on('connection', function connection(app_back,request,client) {
    console.log("client connected")
    let stream,activity_id,client_id,language_code,question,parameters,input_chunks=[];

    app_back.on('message', async function incoming(message) {
      const msg = tryParseJSON(message)
      if(msg && msg.action==="start"){
        console.log("sent start")
        stream = new Readable({
          objectMode: true,
          read() {}
        })
        app_back.send(JSON.stringify({state: "listening"}));
        activity_id = msg.activity_id;
        parameters = msg.parameters;
        client_id = msg.client_id;
        language_code = msg.language_code;

        const speechToText = new SpeechToTextV1({
          authenticator: new IamAuthenticator({
            apikey: process.env.STT_API_KEY
          }),
          serviceUrl : process.env.STT_URL,
          disableSslVerification: true,
        });
        const params = {
          objectMode: true,
          contentType: 'audio/l16;rate=22050;channels=1',
          inactivityTimeout:5,
          model:STT_models[language_code],
          interimResults:true,
          maxAlternatives:1,
          profanityFilter:false,
          smartFormatting:true,
          speakerLabels:false,
          timestamps:false,
          wordConfidence:true,
          processingMetrics:false,
        };
        const recognizeStream = speechToText.recognizeUsingWebSocket(params);
        recognizeStream.on('data', function(event) {
          let result = tryParseJSON(JSON.stringify(event, null, 2))
          if (result!==undefined){
            question = result.results[0].alternatives[0].transcript
            app_back.send(question)
            //console.log(question)
          }
        });
        recognizeStream.on('error', function(event) { onEvent('Error:', event);});
        recognizeStream.on('end', async function(event) {
          onEvent('End:', event);
          //const result = await generateResult(question,client_id,activity_id,language_code,input_chunks)
          const result = await generateResultV2(question,client_id,parameters,language_code,input_chunks)
          app_back.send(JSON.stringify(result));
        });
        recognizeStream.on('close', async function(event) {onEvent('Close:', event);});
        stream.pipe(recognizeStream);
      }else if(msg && msg.action==="stop"){
        console.log("sent stop")
        stream.push(null)
      }else{
        stream.push(message)
        input_chunks.push(message)
      }
    });
  });
}