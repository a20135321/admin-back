const AssistantV2 = require('ibm-watson/assistant/v1');
const { IamAuthenticator } = require('ibm-watson/auth');

const assistant = new AssistantV2({
  version: '2020-04-01',
  authenticator: new IamAuthenticator({
    apikey: process.env.ASSISTANT_API_KEY,
  }),
  serviceUrl: process.env.ASSISTANT_URL,
});
/*
assistant.listWorkspaces()
  .then(res => {
    console.log(JSON.stringify(res.result, null, 2));
  })
  .catch(err => {
    console.log(err)
  });
*/
const workspace_schema = process.env.ASSISTANT_SCHEMA
const work_spaces = {
  test:{
    en:"cdb3618f-acb4-4253-ae4e-961f554074c1",
    es:"44ec3804-abd4-42a0-9d42-0284fc60dc81",
    it:"b51b61a2-dd89-4508-896f-cea30bf73da0"
  },
  dev:{
    en:"94e4a68d-964f-4d01-afcb-ac703b038b7e",
    es:"27f7a734-dcc7-4086-b8f1-05e8e84f7ebb",
    it:"03902183-1743-42cb-aaf8-a04517db6228"
  }
}

const consultWatson = async (language_code,conversation_context,text_to_watson) =>{
  const response = {};
  const params = {
    workspaceId: work_spaces[workspace_schema][language_code],
    input: {'text': text_to_watson}
  }
  if (conversation_context!==null){
    params.context = conversation_context
  }
  const res = await assistant.message(params)
  const actions = res.result.actions

  response.intent = undefined;
  response.text = res.result.output.text[0]
  if (actions!==undefined){
    response.intent = actions[0].parameters.intent
    const target = actions[0].parameters.target
    response.intent += (target!==undefined)?"_"+target:"";
  }
  //response.intent = (res.result.intents[0]!==undefined)?res.result.intents[0].intent:undefined
  //response.entities = res.result.entities
  //response.output = res.result.output.text[0]
  response.context = res.result.context
  return response;
}

const consultWatsonV2 = async (language_code,conversation_context,text_to_watson) =>{
  const params = {
    workspaceId: work_spaces[workspace_schema][language_code],
    input: {'text': text_to_watson}
  }
  if (conversation_context!==null){
    params.context = conversation_context
  }
  const res = await assistant.message(params)
  const actions = res.result.actions?res.result.actions:[]
  return {app_actions:actions.filter(elm=>elm.parameters.target==='app'), back_actions:actions.filter(elm=>elm.parameters.target==='back'),context : res.result.context};
}

const watsonTest = async (language_code,conversation_context,text_to_watson) =>{
  const params = {
    workspaceId: work_spaces[workspace_schema][language_code],
    input: {'text': text_to_watson}
  }
  if (conversation_context!==null){
    params.context = conversation_context
  }
  let res = await assistant.message(params)
  return res.result.actions
}

const getQuestionByIntent = async (intent,language_code) =>{
  const params = {
    workspaceId: work_spaces[workspace_schema][language_code],
    intent: intent,
    _export:true
  };
  try{
    const response = await assistant.getIntent(params)
    return response.result.examples[0].text
  }catch (error){
    return null
  }
}


module.exports = {
  consultWatson,
  consultWatsonV2,
  watsonTest
}


