const TextToSpeechV1 = require('ibm-watson/text-to-speech/v1');
const { IamAuthenticator } = require('ibm-watson/auth');
const {errorMessage,successMessage,status} = require('../../Helpers/status');
const cloneDeep = require('clone-deep');

const TTS_models = {
  en:"en-US_LisaV3Voice",
  es:"es-ES_LauraV3Voice",
  it:"it-IT_FrancescaV3Voice"
}

const synthesize = async (req,res) => {
  const eMessage = cloneDeep(errorMessage)
  const text = req.query.text
  try {
    if (text===undefined || text===''){
      eMessage.errors.push({type:'text',msg:'Text to synthesize is empty'});
      return res.status(status.error).send(eMessage);
    }
    const format = req.query.format
    const textToSpeech = new TextToSpeechV1({
      authenticator: new IamAuthenticator({
        apikey: process.env.TTS_API_KEY
      }),
      serviceUrl: process.env.TTS_URL,
    });
    const synthesizeParams = {
      accept: 'audio/'+format,
      voice: TTS_models[req.query.language_code],
      text:text
    };
    const { result } = await textToSpeech.synthesize(synthesizeParams)
    res.type(format)
    result.pipe(res)
    return res.status(status.success);
  } catch (error) {
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

module.exports = {
  synthesize
}