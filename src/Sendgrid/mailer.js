const sg = require('@sendgrid/mail')

sg.setApiKey(process.env.SENDGRID_API_KEY)

const sendEmailNotification = (message, email) =>{
  const mail_body = {
    to:email,
    from:process.env.SENDGRID_EMAIL,
    subject:"REBECA ADMIN - NOTIFICATION",
    text:message,
  }
  try {
    sg.send(mail_body)
  }catch (e){
    console.log(e)
  }
}

module.exports = {
  sendEmailNotification,
}
