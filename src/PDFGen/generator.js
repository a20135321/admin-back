const pdf = require('html-pdf');
const fetch = require('node-fetch');
const dbQuery  = require('../config/datos-relacionales')
const ejs = require('ejs');
const fs = require('fs');

const imageURL2Buffer= async (url) =>{
  const res = await fetch(url,{encoding: null });
  const imageBuffer = await res.buffer();
  return new Buffer.from(imageBuffer, 'base64');
}

const getUserTripData = async (user_trip_id) =>{
  let data={},dbResponse;
  try {
    const queryText1 = "select ai.start_time,a.image,a.title,a.duration,as2.date from user_activity ua inner join activity_instance ai" +
      " on ua.activ_instance_id=ai.activ_instance_id inner join activity_schedule as2 on ai.activ_schedule_id=as2.activ_schedule_id" +
      " inner join activity a on ai.activity_id=a.activity_id where ua.user_trip_id = $1"
    const queryText2 = "select ea.description,ea.start_time,ea.end_time,ea.date,a.activity_id,a.title,a.image from external_activity ea left join activity a" +
      " on ea.associated_activity_id=a.activity_id where user_trip_id = $1"
    dbResponse = await dbQuery.query(queryText1,[user_trip_id])
    data.activities = (dbResponse.rows[0]===undefined)?[]:dbResponse.rows;
    dbResponse = await dbQuery.query(queryText2,[user_trip_id])
    data.other_activities = (dbResponse.rows[0]===undefined)?[]:dbResponse.rows;
    return data;
  } catch (error) {
    return undefined;
  }
}

const generatePDF = async (req, res, next) =>{

  const user_trip_id = req.params.user_trip_id;
  const data = await getUserTripData(user_trip_id);
  console.log(data)

  res.writeHead(200, {
    'Content-Type': 'application/pdf',
    'Content-disposition': 'attachment;filename=YourSchedule.pdf'
  });

  const content = `
<!doctype html>
    <html>
       <head>
            <meta charset="utf-8">
            <title>PDF Result Template</title>
            <style>
                h1 {
                    color: green;
                }
            </style>
        </head>
        <body>
            <h1>Título en el PDF creado con el paquete html-pdf</h1>
            <p>Generando un PDF con un HTML sencillo</p>
        </body>
    </html>
`;

  const compiled = ejs.compile(fs.readFileSync('src/PDFGen/template.html', 'utf8'));
  const html = compiled({ title : 'EJS', holi1 : 'Hello, World!' , holi2 : 'This is a holi2!' });

  pdf.create(html).toStream(function(err, stream){
    stream.pipe(res);
  });
  /*
  pdf.create(content).toStream(function(err, stream){
    stream.pipe(res);
  });

   */
}

module.exports = {
  generatePDF,
}