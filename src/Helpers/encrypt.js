const {errorMessage,successMessage,status} = require('../Helpers/status');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const cloneDeep = require('clone-deep');

const comparePassword = (password,real_password) =>{
  return bcrypt.compareSync(password, real_password);
}

const hash = async (password) =>{
  return await bcrypt.hash(password, 10)
}

const generateJWT = (user) =>{
  let payload = {
    user_id: user.user_id,
    email: user.email,
    name: user.name
  };
  return jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: '1h'});
}

const refreshJWT = async (req, res) =>{
  const eMessage = cloneDeep(errorMessage)
  const sMessage = cloneDeep(successMessage)
  const body = req.body.data;
  try {
    const payload = jwt.verify(body.token, process.env.TOKEN_SECRET);
    delete payload.iat;
    delete payload.exp;
    delete payload.nbf;
    const new_token = jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: '30m'});
    sMessage.data.token = new_token
    return res.status(status.success).send(sMessage);
  }catch (error){
    console.log(error)
    return res.status(status.error).send(eMessage);
  }
}

module.exports = {
  hash,
  comparePassword,
  generateJWT,
  refreshJWT
}