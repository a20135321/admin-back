const successMessage = { status: 'success', data:{} };
const errorMessage = { status: 'error' , errors:[{type:'generic',msg:'An error occurred! Try again later'}] };
const status = {
  success: 200,
  error: 500,
  notfound: 404,
  unauthorized: 401,
  conflict: 409,
  created: 201,
  bad: 400,
  noContent: 204,
};

module.exports = {
  successMessage,
  errorMessage,
  status
};